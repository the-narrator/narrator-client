const httpClient = require('../clients/httpClient');

const { MAX_PLAYER_COUNT } = require('../constants/narratorConstants');


async function update(schema){
    if(!schema.modifiers)
        return;
    return Promise.all(
        schema.modifiers.map(modifier => ({
            minPlayerCount: 0,
            maxPlayerCount: MAX_PLAYER_COUNT,
            ...modifier,
        })).map(modifier => httpClient.post('/api/setupModifiers', modifier)),
    );
}

module.exports = {
    update,
};
