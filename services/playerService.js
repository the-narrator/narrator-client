const httpClient = require('../clients/httpClient');


function addOtherPlayer(joinID, authToken){
    const headers = { auth: authToken, authType: 'tempAuth' };
    return httpClient.post('/api/players', { joinID }, headers);
}

module.exports = {
    addOtherPlayer,
};
