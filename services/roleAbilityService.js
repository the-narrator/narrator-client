const httpClient = require('../clients/httpClient');

const { MAX_PLAYER_COUNT } = require('../constants/narratorConstants');


function modify(roleID, roleAbilityID, modifier){
    return httpClient.post(`/api/roles/${roleID}/abilities/${roleAbilityID}/modifiers`, {
        minPlayerCount: 0,
        maxPlayerCount: MAX_PLAYER_COUNT,
        ...modifier,
    });
}

module.exports = {
    modify,
};
