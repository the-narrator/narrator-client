const httpClient = require('../clients/httpClient');

function clearCache(threadID){
    const url = `/api/channels/sc2mafia/condorcet?=${threadID}`;
    return httpClient.delete(url);
}

module.exports = {
    clearCache,
};
