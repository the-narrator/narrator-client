const { keyBy } = require('lodash');
const httpClient = require('../clients/httpClient');


function createHiddens(factionRoles, factions, schema, setupID){
    const factionRoleMap = keyBy(factionRoles, 'name');
    const factionMap = keyBy(factions, 'name');
    const hiddenMap = keyBy(schema.hiddens || [], 'name');
    const requests = {};
    schema.setupHiddens.forEach(setupHidden => {
        const hidden = hiddenMap[setupHidden.name];
        if(hidden){
            requests[`hidden-${hidden.name}`] = () => createHidden(
                hidden.name,
                hidden.spawns.map(spawn => {
                    if(!spawn.role || !spawn.faction)
                        return {
                            factionRoleID: factionRoleMap[spawn.name].id,
                            ...spawn,
                        };
                    return getSpecificFactionRole(spawn, factionMap, factionRoles);
                }),
                setupID,
            );
        }else if(!setupHidden.role || !setupHidden.faction){
            const factionRole = factionRoleMap[setupHidden.name];
            requests[`role-${setupHidden.name}`] = () => createHidden(
                setupHidden.name,
                [{ factionRoleID: factionRole.id }],
                setupID,
            );
        }else{
            const requestHash = `role-${setupHidden.role.name},faction-${setupHidden.faction.name}`;
            requests[requestHash] = () => createHidden(
                setupHidden.role.name,
                [getSpecificFactionRole(setupHidden, factionMap, factionRoles)],
                setupID,
            );
        }
    });
    return Promise.all(Object.values(requests).map(f => f()));
}

function deleteAll(hiddenIDs){
    return Promise.all(hiddenIDs.map(deleteHidden));
}

module.exports = {
    createHiddens,
    deleteAll,
};

function getSpecificFactionRole(spawn, factionMap, factionRoles){
    const roleName = spawn.role.name;
    const factionID = factionMap[spawn.faction.name].id;
    const factionRoleID = factionRoles
        .find(fr => fr.factionID === factionID && fr.name === roleName).id;
    return {
        ...spawn,
        factionRoleID,
    };
}

async function createHidden(name, spawns, setupID){
    const serverResponse = await httpClient.post('/api/hiddens', { name, setupID });
    const hidden = JSON.parse(serverResponse).response;
    spawns = spawns.map(spawn => ({
        minPlayerCount: 'minPlayerCount' in spawn ? spawn.minPlayerCount : 0,
        maxPlayerCount: spawn.maxPlayerCount || 255,
        ...spawn,
        hiddenID: hidden.id,
    }));
    await httpClient.post('/api/hiddenSpawns', spawns);
    return {
        ...hidden,
        spawns,
    };
}

function deleteHidden(hiddenID){
    return httpClient.delete(`/api/hiddens/${hiddenID}`);
}
