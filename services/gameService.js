const httpClient = require('../clients/httpClient');


async function createGame(setupID){
    const response = await httpClient.post('/api/games', { setupID });
    return JSON.parse(response).response;
}

async function getAllGames(){
    const response = await httpClient.get('/api/games');
    return JSON.parse(response).response;
}

async function getByID(gameID){
    const response = await httpClient.get(`/api/games/${gameID}`);
    return JSON.parse(response).response;
}

function killGame(joinID){
    return httpClient.delete(`/api/games/${joinID}`);
}

function setSetup(gameID, setupID){
    return httpClient.put('/api/setups', { gameID, setupID });
}

module.exports = {
    createGame,
    getAllGames,
    getByID,
    getByJoinID,
    killGame,
    setSetup,
};
