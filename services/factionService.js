const httpClient = require('../clients/httpClient');

const { MAX_PLAYER_COUNT } = require('../constants/narratorConstants');

const helpers = require('../util/helpers');


async function createFactionShell(args){
    const factionServerResponse = await httpClient.post('/api/factions', args);
    return JSON.parse(factionServerResponse).response;
}

async function addEnemies(factions, schema){
    const factionMap = helpers.makeMap(factions, 'name');
    return schema.enemies.map(([enemy1, enemy2]) => addEnemy(
        factionMap[enemy1.name].id, factionMap[enemy2.name].id,
    ));
}

async function addFactionAbility(factionID, abilityName){
    const response = await httpClient.post(`/api/factions/${factionID}/abilities`, {
        ability: abilityName,
    });
    return JSON.parse(response).response;
}

function addSheriffDetectable(factionID, detectableFactionID){
    return httpClient.post(
        `/api/factions/${factionID}/checkables/${detectableFactionID}`,
    );
}

async function createFactionRole(roleID, factionID){
    const serverResponse = await httpClient.post('/api/factionRoles', { roleID, factionID });
    return JSON.parse(serverResponse).response;
}

function setWinPriorities(factions, winPriority){
    const factionMap = helpers.makeMap(factions, 'name');
    winPriority = [...winPriority];
    winPriority.reverse();
    return Promise.all(winPriority
        .map((priority, index) => {
            const winValue = index + 1;
            if(!Array.isArray(priority))
                return setWinPriority(factionMap[priority.name].id, winValue);
            return Promise.all(priority.map(subPriority => setWinPriority(
                factionMap[subPriority.name].id, winValue,
            )));
        }));
}

function modify(factionID, modifier){
    return httpClient.post(`/api/factions/${factionID}/modifiers`, {
        minPlayerCount: 0,
        maxPlayerCount: MAX_PLAYER_COUNT,
        ...modifier,
    });
}

function deleteAll(factionIDs){
    return Promise.all(factionIDs.map(deleteFaction));
}

module.exports = {
    createFactionShell,
    createFactionRole,
    addEnemies,
    addFactionAbility,
    addSheriffDetectable,
    setWinPriorities,
    modify,
    deleteAll,
};

function addEnemy(factionID, enemyFactionID){
    return httpClient.post(`/api/factions/${factionID}/enemies/${enemyFactionID}`);
}

function setWinPriority(factionID, priorityValue){
    return httpClient.post(`/api/factions/${factionID}/modifiers`, {
        name: 'WIN_PRIORITY',
        value: priorityValue,
        minPlayerCount: 0,
        maxPlayerCount: MAX_PLAYER_COUNT,
    });
}

function deleteFaction(factionID){
    return httpClient.delete(`/api/factions/${factionID}`);
}
