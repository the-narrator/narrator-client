const httpClient = require('../clients/httpClient');


async function getTempAuthToken(userID){
    const serverResponse = await httpClient.post(`/api/users/${userID}/auth_token`);
    return JSON.parse(serverResponse).response;
}

async function getUsers(){
    const serverResponse = await httpClient.get('/api/users');
    return JSON.parse(serverResponse).response;
}

module.exports = {
    getTempAuthToken,
    getUsers,
};
