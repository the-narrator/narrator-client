const httpClient = require('../clients/httpClient');

const { MAX_PLAYER_COUNT } = require('../constants/narratorConstants');


function modify(factionAbilityID, modifier, minPlayerCount = 0, maxPlayerCount = MAX_PLAYER_COUNT){
    return httpClient.post(`/api/factionAbilities/${factionAbilityID}/modifiers`, {
        minPlayerCount,
        maxPlayerCount,
        ...modifier,
    });
}

module.exports = {
    modify,
};
