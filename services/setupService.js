const httpClient = require('../clients/httpClient');


async function create({ name, description, slogan }){
    const response = await httpClient.post('/api/setups', { name, description, slogan });
    return JSON.parse(response).response;
}

async function getByID(setupID){
    const response = await httpClient.get(`/api/setups/${setupID}`);
    return JSON.parse(response).response;
}

async function getFeaturedSetups(){
    const response = await httpClient.get('/api/setups/featured');
    return JSON.parse(response).response;
}

async function getUserSetups(userID){
    const response = await httpClient.get(`/api/setups?userID=${userID}`);
    return JSON.parse(response).response;
}

function setFeatured(setupID, priority, key){
    return httpClient.post(`/api/setups/${setupID}/featured`, { priority, key });
}

function removeFeatured(setupID){
    return httpClient.delete(`/api/setups/${setupID}/featured`);
}

function deleteSetup(setupID){
    return httpClient.delete(`/api/setups/${setupID}`);
}

module.exports = {
    create,
    getByID,
    getFeaturedSetups,
    getUserSetups,
    setFeatured,
    removeFeatured,
    deleteSetup,
};
