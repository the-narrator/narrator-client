const httpClient = require('../clients/httpClient');
const { MAX_PLAYER_COUNT } = require('../constants/narratorConstants');


async function createRole(roleSchema){
    const serverResponse = await httpClient.post('/api/roles', roleSchema);
    return JSON.parse(serverResponse).response;
}

function modify(roleID, modifierArgs){
    return httpClient.post(`/api/roles/${roleID}/modifiers`, {
        minPlayerCount: 0,
        maxPlayerCount: MAX_PLAYER_COUNT,
        ...modifierArgs,
    });
}

function deleteAll(roleIDs){
    return Promise.all(roleIDs.map(deleteRole));
}

module.exports = {
    createRole,
    modify,
    deleteAll,
};

function deleteRole(roleID){
    return httpClient.delete(`/api/roles/${roleID}`);
}
