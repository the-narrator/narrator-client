const httpClient = require('../clients/httpClient');


async function getProfile(){
    const response = await httpClient.get('/api/profiles');
    return JSON.parse(response).response;
}

module.exports = {
    getProfile,
};
