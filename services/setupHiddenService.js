const httpClient = require('../clients/httpClient');

const helpers = require('../util/helpers');

const { MAX_PLAYER_COUNT } = require('../constants/narratorConstants');

async function addSetupHiddens(hiddens, schema){
    const map = helpers.makeMap(hiddens, 'name');
    return Promise.all(
        schema.setupHiddens.map(({
            minPlayerCount, isExposed, mustSpawn, maxPlayerCount, ...setupHidden
        }) => {
            if(setupHidden.role && setupHidden.faction){
                const hidden = hiddens.filter(h => h.spawns.length === 1 && h.spawns[0].faction)
                    .find(h => h.spawns[0].faction.name === setupHidden.faction.name
                        && h.spawns[0].role.name === setupHidden.role.name);
                return addSetupHidden(hidden.id, minPlayerCount, maxPlayerCount, isExposed,
                    mustSpawn);
            }

            const hidden = map[setupHidden.name];
            return addSetupHidden(hidden.id, minPlayerCount, maxPlayerCount, isExposed, mustSpawn);
        }),
    );
}

module.exports = {
    addSetupHiddens,
};

function addSetupHidden(hiddenID, minPlayerCount = 0, maxPlayerCount = MAX_PLAYER_COUNT,
    isExposed = false, mustSpawn = false){
    return httpClient.post('/api/setupHiddens', {
        hiddenID, isExposed, minPlayerCount, maxPlayerCount, mustSpawn,
    });
}
