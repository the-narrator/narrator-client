const httpClient = require('../clients/httpClient');

const { MAX_PLAYER_COUNT } = require('../constants/narratorConstants');


function modifyAbility(factionRoleID, abilityID, modifier){
    return httpClient.post(`/api/factionRoles/${factionRoleID}/abilities/${abilityID}/modifiers`, {
        minPlayerCount: 0,
        maxPlayerCount: MAX_PLAYER_COUNT,
        ...modifier,
    });
}

function modifyRole(factionRoleID, modifier){
    return httpClient.post(`/api/factionRoles/${factionRoleID}/modifiers`, {
        minPlayerCount: 0,
        maxPlayerCount: MAX_PLAYER_COUNT,
        ...modifier,
    });
}

function setReceived(factionRoleID, receivedFactionRoleID){
    return httpClient.put(`/api/factionRoles/${factionRoleID}`, { receivedFactionRoleID });
}

module.exports = {
    modifyAbility,
    modifyRole,
    setReceived,
};
