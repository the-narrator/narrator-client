const narratorEnv = process.argv[2] || 'local';

function getHost(){
    if(narratorEnv === 'prod')
        return 'thenarrator.net';
    if(narratorEnv === 'dev')
        return 'narrator.systeminplace.net';
    return 'localhost';
}

function getPort(){
    if(narratorEnv === 'local' || narratorEnv === 'nofirebase')
        return 4501;
    if(narratorEnv === 'dev')
        return 4000;
    return 443;
}

function getBaseURL(){
    if(narratorEnv === 'prod')
        return 'thenarrator.net';
    return 'localhost:4502';
}

module.exports = {
    baseURL: getBaseURL(),
    getEnv: () => narratorEnv,
    host: getHost(),
    port: getPort(),
};
