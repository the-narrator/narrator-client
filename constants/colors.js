module.exports = {
    blue: '#6495ED',
    brown: '#CD853E',
    green: '#00FF00',
    orange: '#FFA500',
    pink: '#DDA0DD',
    purple: '#880BFC',
    red: '#FF0000',
    sicklyGreen: '#D5E68C',
    yellow: '#FFFF00',
};
