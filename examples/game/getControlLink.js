const { baseURL } = require('../../config');

const userService = require('../../services/userService');


async function main(){
    const token = await userService.getTempAuthToken(151);
    const url = `${baseURL}?auth_token=${token}`;
    console.log(url);
}

main().catch(console.log).then(process.exit);
