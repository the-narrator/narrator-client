const { baseURL } = require('../../config');

const gameService = require('../../services/gameService');
const userService = require('../../services/userService');


async function main(){
    const joinID = process.argv[3];
    if(!joinID)
        return console.log('specify joinID please');

    const game = await gameService.getByJoinID(joinID);
    const authRequests = game.players
        .map(player => userService.getTempAuthToken(player.userID).then(token => [token, player]));
    const tokens = await Promise.all(authRequests);
    tokens.forEach(([token, { name, factionRole }]) => {
        console.log(name, factionRole && factionRole.team.name, factionRole && factionRole.name);
        console.log(`${baseURL}?auth_token=${token}`);
    });
}

main().catch(console.log).then(process.exit);
