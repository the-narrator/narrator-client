const gameService = require('../../services/gameService');

async function main(){
    const joinID = process.argv[3];
    if(joinID)
        return gameService.killGame(joinID);
}

main().catch(console.log).then(process.exit);
