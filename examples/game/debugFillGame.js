const gameService = require('../../services/gameService');
const playerService = require('../../services/playerService');
const userService = require('../../services/userService');


async function main(){
    const [game] = await gameService.getAllGames();
    if(!game)
        return console.log('no games found');
    const playersToAddCount = getPlayerCount(game);
    const allUsers = await userService.getUsers();
    const authTokensPromise = allUsers
        .map(({ id }) => id)
        .filter(userID => game.host.id !== userID)
        .slice(0, playersToAddCount)
        .map(userService.getTempAuthToken);
    const authTokens = await Promise.all(authTokensPromise);
    await Promise.all(authTokens
        .map(authToken => playerService.addOtherPlayer(game.joinID, authToken)));
}

main().catch(console.error).then(process.exit);

function getPlayerCount(game){
    const playerCount = process.argv[3];
    if(!playerCount)
        return game.setup.maxPlayerCount - 1;
    return parseInt(playerCount, 10);
}
