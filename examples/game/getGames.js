const gameService = require('../../services/gameService');


async function main(){
    const games = await gameService.getAllGames();
    console.log('JoinID\thostID\tstarted\tdayNumber\tsize');
    console.log(games.map(game => [
        game.joinID,
        game.isStarted,
        `${game.dayNumber}\t`,
        game.players.length,
    ].join('\t')).join('\n'));
}

main().catch(console.log).then(process.exit);
