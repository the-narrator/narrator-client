const gameService = require('../services/gameService');


async function main(){
    const setupID = 2610;
    return gameService.createGame(setupID);
}

main().catch(console.log).then(process.exit);
