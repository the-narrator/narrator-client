const setupService = require('../../services/setupService');


async function main(){
    const userID = 426;
    const userSetups = await setupService.getUserSetups(userID);
    return Promise.all(userSetups.map(setup => setupService.deleteSetup(setup.id)));
}

main().catch(console.log).then(process.exit);
