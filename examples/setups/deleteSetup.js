const setupClient = require('../../services/setupService');

async function main(){
    return setupClient.deleteSetup(process.argv[3]);
}

main().catch(console.log).then(process.exit);
