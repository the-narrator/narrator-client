const factionService = require('../.././services/factionService');
const gameService = require('../.././services/gameService');
const hiddenService = require('../.././services/hiddenService');
const profileService = require('../.././services/profileService');
const setupService = require('../.././services/setupService');
const setupHiddenService = require('../.././services/setupHiddenService');
const setupModifierService = require('../.././services/setupModifierService');

const schemaUtil = require('../.././util/schemaUtil');


async function main(){
    const profile = await profileService.getProfile();
    const setups = await setupService.getUserSetups(profile.userID);
    const gameID = await getInGame(profile, setups);
    await constructSetup(gameID, setups);
}

// eslint-disable-next-line no-console
main().catch(console.log).then(process.exit);

async function constructSetup(gameID, currentSetups){
    const { schema } = require('../.././featuredSetups/gikkle');
    const setup = await schemaUtil.getOrCreateSetup(currentSetups, schema);
    await gameService.setSetup(gameID, setup.id);
    const [factions, , roles] = await Promise.all([
        schemaUtil.createFactions(setup, schema.factions),
        hiddenService.deleteAll(setup.hiddens.map(h => h.id)),
        schemaUtil.createRoles(setup, schema.roles),
        setupModifierService.update(schema),
    ]);
    await Promise.all([
        schemaUtil.createFactionRoles(factions, roles, schema)
            .then(factionRoles => hiddenService.createHiddens(
                factionRoles, factions, schema, setup.id,
            ))
            .then(hiddens => setupHiddenService.addSetupHiddens(hiddens, schema)),
        factionService.setWinPriorities(factions, schema.winPriority),
        factionService.addEnemies(factions, schema),
        schemaUtil.setSheriffDetectables(factions, schema.sheriffDetectables),
    ]);
    if(schema.featuredPriority)
        return setupService.setFeatured(setup.id, schema.featuredPriority, schema.featuredKey);
    console.log('setupID is ' + setup.id);
}

async function getInGame(profile, setups){
    if(profile.gameID)
        return profile.gameID;
    const setupID = setups.length ? setups[0].id
        : (await setupService.create({ name: 'filler setup' })).id;
    const game = await gameService.createGame(setupID);
    return game.id;
}
