const setupService = require('../../services/setupService');


async function main(){
    const calls = [];
    for(let i = 0; i < 10; i++){
        const start = new Date().getTime();
        await setupService.getByID(process.argv[3]);
        const end = new Date().getTime();
        console.log(end - start);
        calls.push(end - start);
    }
    console.log('avg', sum(calls) / calls.length);
    // console.log(JSON.stringify(setup, null, 4));
}

main().catch(console.log).then(process.exit);

function sum(arr){
    return arr.reduce((a, b) => a + b, 0);
}
