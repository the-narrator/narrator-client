const channelService = require('../../services/channelService');

async function main(){
    const threadID = '';
    return channelService.clearCache(threadID);
}

main().catch(console.error).then(process.exit);
