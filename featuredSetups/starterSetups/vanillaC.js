const { getSetupHidden } = require('../../util/playerCountHelpers');

const setupModifiers = require('../../constants/setupModifiers');

const { town, mafia } = require('../definitions/baseFactions');

const {
    agent, citizen, detective, doctor, goon, lookout, sheriff, stripper,
} = require('../definitions/baseRoles');


const slogan = 'Classic Setup with basic confirmed roles';
const description = 'New Mafia Role - Agent\nTown roles are randomly generated';

const townPowerRole = {
    name: 'Hidden Town',
    factionRoles: [stripper, sheriff, doctor, lookout, detective],
    isExposed: true,
};

const schema = {
    description,
    enemies: [[town, mafia]],
    factions: [town, mafia],
    factionRoles: {
        [town.name]: [citizen, detective, doctor, lookout, sheriff, stripper],
        [mafia.name]: [agent, goon],
    },
    hiddens: [townPowerRole],
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: false,
    }, {
        name: setupModifiers.FOLLOW_GETS_ALL,
        value: false,
    }, {
        name: setupModifiers.BLOCK_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_SUCCESS_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_FEEDBACK,
        value: false,
    }],
    name: 'Vanilla C',
    roles: [agent, citizen, detective, doctor, goon, lookout, sheriff, stripper],
    setupHiddens: [
        agent,
        goon,
        citizen,
        citizen,
        citizen,
        townPowerRole,
        townPowerRole,
        getSetupHidden(citizen, 8),
        getSetupHidden(citizen, 9),
        getSetupHidden(goon, 10),
        getSetupHidden(townPowerRole, 11),
        getSetupHidden(citizen, 12),
        getSetupHidden(goon, 13),
        getSetupHidden(citizen, 14, 14),
        getSetupHidden(agent, 15),
        getSetupHidden(townPowerRole, 15),
        getSetupHidden(citizen, 16),
    ],
    sheriffDetectables: {
        [town.name]: [mafia],
    },
    slogan,
    winPriority: [mafia, town],
};

module.exports = {
    schema,
};
