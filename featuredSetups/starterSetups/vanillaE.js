const { getSetupHidden } = require('../../util/playerCountHelpers');

const setupModifiers = require('../../constants/setupModifiers');

const {
    town, mafia, threat, benigns,
} = require('../definitions/baseFactions');

const {
    agent, blackmailer, citizen, detective, doctor, goon, jester, lookout, sheriff, stripper,
} = require('../definitions/baseRoles');


const slogan = 'Basic game with 3rd party roles';
const description = 'New Neutral Roles - Serial Killer and Jester\nMinimum of 8 players';

const mafiaPowerRole = {
    name: 'Hidden Mafia',
    spawns: [agent, blackmailer],
};

const serialKiller = {
    name: 'Serial Killer',
    abilities: ['SerialKiller', 'Bulletproof'],
};

const townPowerRole = {
    name: 'Hidden Town',
    spawns: [stripper, sheriff, doctor, lookout, detective],
};

const schema = {
    description,
    enemies: [[town, mafia], [mafia, threat], [threat, town]],
    factions: [benigns, threat, town, mafia],
    spawns: {
        [benigns.name]: [jester],
        [town.name]: [citizen, detective, doctor, lookout, sheriff, stripper],
        [threat.name]: [serialKiller],
        [mafia.name]: [agent, blackmailer, goon],
    },
    hiddens: [mafiaPowerRole, townPowerRole],
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: false,
    }, {
        name: setupModifiers.FOLLOW_GETS_ALL,
        value: false,
    }, {
        name: setupModifiers.BLOCK_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_SUCCESS_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.JESTER_CAN_ANNOY,
        value: false,
    }, {
        name: setupModifiers.JESTER_KILLS,
        value: 0,
    }],
    name: 'Silence of the Lambs',
    roles: [
        agent, blackmailer, citizen, detective, doctor, goon, jester, lookout, sheriff,
        serialKiller, stripper,
    ],
    setupHiddens: [
        citizen,
        citizen,
        citizen,
        townPowerRole,
        townPowerRole,
        mafiaPowerRole,
        getSetupHidden(mafiaPowerRole, 0, 11),
        getSetupHidden(jester, 0, 9),
        getSetupHidden(citizen, 9),
        getSetupHidden(serialKiller, 10),
        getSetupHidden(citizen, 10),
        getSetupHidden(jester, 11),
        getSetupHidden(goon, 12),
        getSetupHidden(goon, 12, 12),
        getSetupHidden(townPowerRole, 13),
        getSetupHidden(mafiaPowerRole, 13),
        getSetupHidden(citizen, 14, 15),
        getSetupHidden(jester, 15),
        getSetupHidden(goon, 16),
        getSetupHidden(townPowerRole, 16),
    ],
    sheriffDetectables: {
        [town.name]: [mafia],
    },
    slogan,
    winPriority: [threat, mafia, town, benigns],
};

module.exports = {
    schema,
};
