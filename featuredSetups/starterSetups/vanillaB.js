const { getSetupHidden } = require('../../util/playerCountHelpers');

const setupModifiers = require('../../constants/setupModifiers');

const { town, mafia } = require('../definitions/baseFactions');

const {
    citizen, detective, goon, lookout, stripper,
} = require('../definitions/baseRoles');


const slogan = 'Blocker/Lookout setup';
const description = 'New Town Roles - Lookout, Detective Stripper\n'
    + 'Mafia must decide who does the killing';

const schema = {
    description,
    enemies: [[town, mafia]],
    factions: [town, mafia],
    factionRoles: {
        [town.name]: [citizen, detective, lookout, stripper],
        [mafia.name]: [goon],
    },
    hiddens: [],
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: false,
    }, {
        name: setupModifiers.FOLLOW_GETS_ALL,
        value: false,
    }, {
        name: setupModifiers.BLOCK_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_SUCCESS_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_FEEDBACK,
        value: false,
    }],
    name: 'Vanilla B',
    roles: [citizen, detective, goon, lookout, stripper],
    setupHiddens: [
        goon,
        goon,
        citizen,
        citizen,
        citizen,
        stripper,
        lookout,
        getSetupHidden(citizen, 8),
        getSetupHidden(citizen, 9),
        getSetupHidden(goon, 10),
        getSetupHidden(detective, 11),
        getSetupHidden(citizen, 12),
        getSetupHidden(goon, 13),
        getSetupHidden(citizen, 14, 14),
        getSetupHidden(goon, 15),
        getSetupHidden(detective, 15),
        getSetupHidden(citizen, 16),
    ],
    sheriffDetectables: {},
    slogan,
    winPriority: [mafia, town],
};

module.exports = {
    schema,
};
