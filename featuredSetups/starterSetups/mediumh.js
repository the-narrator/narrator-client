const { getSetupHidden } = require('../../util/playerCountHelpers');

const setupModifiers = require('../../constants/setupModifiers');

const {
    mafia, town, threat,
} = require('../definitions/baseFactions');

const {
    citizen, stripper, sheriff, doctor, lookout, detective, gunsmith, mayor, snitch,
    assassin, goon, agent, blackmailer,
} = require('../definitions/baseRoles');


const slogan = 'Day action mafia';
const description = 'New Town Roles - Mayor, Snitch, Gunsmith\n'
    + 'New Mafia Role - Assassin\n'
    + 'New Neutral Roles - Arsonist, Poisoner\n'
    + 'Guns are shot during the day\n'
    + 'Arsonist can ignite once during the day\n';

const serialKiller = {
    name: 'Serial Killer',
    abilities: [{
        name: 'SerialKiller',
    }, {
        name: 'Bulletproof',
    }],
};

const arsonist = {
    name: 'Arsonist',
    abilities: ['Douse', 'Burn', 'Undouse', 'Bulletproof'],
};

const poisoner = {
    name: 'Poisoner',
    abilities: ['Poisoner', 'Bulletproof'],
};

const townPowerRole = {
    name: 'Hidden Town',
    spawns: [
        stripper, sheriff, doctor, lookout, detective, gunsmith, mayor, snitch,
    ],
};

const mafiaPowerRole = {
    name: 'Hidden Mafia',
    spawns: [
        agent, blackmailer,
    ],
};

const hiddenThreat = {
    name: 'Hidden Killer',
    spawns: [
        serialKiller, poisoner, arsonist,
    ],
};

const schema = {
    description,
    enemies: [[mafia, threat], [threat, town], [mafia, town]],
    factions: [threat, town, mafia],
    factionRoles: {
        [town.name]: [
            citizen, stripper, sheriff, doctor, lookout, detective,
            gunsmith, mayor, snitch,
        ],
        [mafia.name]: [
            assassin, agent, blackmailer, goon,
        ],
        [threat.name]: [serialKiller, poisoner, arsonist],
    },
    hiddens: [townPowerRole, mafiaPowerRole, hiddenThreat],
    modifiers: [{
        name: setupModifiers.ARSON_DAY_IGNITES,
        value: 1,
    }, {
        name: setupModifiers.GS_DAY_GUNS,
        value: true,
    }, {
        name: setupModifiers.DAY_START,
        value: false,
    }, {
        name: setupModifiers.FOLLOW_GETS_ALL,
        value: false,
    }, {
        name: setupModifiers.BLOCK_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_SUCCESS_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_FEEDBACK,
        value: true,
    }],
    name: 'Day Action Mafia',
    roles: [
        citizen, assassin, goon, stripper, gunsmith, mayor, snitch, sheriff, doctor, lookout,
        detective, blackmailer, agent, poisoner, serialKiller, arsonist,
    ],
    setupHiddens: [
        citizen,
        citizen,
        citizen,
        citizen,
        townPowerRole,
        townPowerRole,
        assassin,
        getSetupHidden(goon, 0, 12),
        getSetupHidden(citizen, 9),
        getSetupHidden(hiddenThreat, 10),
        getSetupHidden(citizen, 11, 11),
        getSetupHidden(townPowerRole, 12),
        getSetupHidden(mafiaPowerRole, 12),
        getSetupHidden(townPowerRole, 13),
        getSetupHidden(mafiaPowerRole, 13),
        getSetupHidden(citizen, 14),
        getSetupHidden(goon, 15, 15),
        getSetupHidden(townPowerRole, 16),
        getSetupHidden(mafiaPowerRole, 16),
    ],
    sheriffDetectables: {
        [town.name]: [threat, mafia],
    },
    slogan,
    winPriority: [threat, mafia, town],
};

module.exports = {
    schema,
};
