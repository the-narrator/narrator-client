const { getSetupHidden } = require('../../util/playerCountHelpers');

const abilityModifiers = require('../../constants/abilityModifiers');
const setupModifiers = require('../../constants/setupModifiers');

const {
    town, mafia: mafiaBase, outcasts, benigns, yakuza: yakuzaBase,
} = require('../definitions/baseFactions');

const {
    armorsmith, stripper, gunsmith, sheriff, doctor, lookout, detective, agent, blackmailer,
    framer, executioner, jester, citizen, goon,
} = require('../definitions/baseRoles');


const mafYakAbilities = [{
    name: 'FactionKill',
    modifiers: [{
        name: abilityModifiers.COOLDOWN,
        value: 1,
        minPlayerCount: 9,
        maxPlayerCount: 13,
    }],
}];

const mafia = {
    ...mafiaBase,
    abilities: mafYakAbilities,
};

const yakuza = {
    ...yakuzaBase,
    abilities: mafYakAbilities,
};

const blacksmith = {
    name: 'Blacksmith',
    abilities: [{
        name: 'Blacksmith',
        modifiers: [{
            name: abilityModifiers.AS_FAKE_VESTS,
            value: true,
        }, {
            name: abilityModifiers.GS_FAULTY_GUNS,
            value: true,
        }],
    }],
};

const slogan = 'Intermediate setup with two mafia teams';
const description = 'New Town Roles - Gunsmith, Armorsmith\n'
    + 'New Mafia Roles - Framer, Stripper, Gunsmith, Armorsmith\n'
    + 'New Neutral Roles - Gunsmith, Armorsmith\n'
    + 'Nontown Armorsmith and Gunsmith items can be fake\n'
    + 'Mafia can only kill every night if starting player size was at least 14';

const mafiaYakuzaHiddenSpawns = [agent, blackmailer, framer, blacksmith, stripper];

const mafiaPowerRole = {
    name: 'Hidden Mafia',
    spawns: mafiaYakuzaHiddenSpawns.map(role => ({ role, faction: mafia })),
};

const yakuzaPowerRole = {
    name: 'Hidden Yakuza',
    spawns: mafiaYakuzaHiddenSpawns.map(role => ({ role, faction: yakuza })),
};

const townPowerRole = {
    name: 'Hidden Town',
    spawns: [
        { role: stripper, faction: town }, armorsmith, gunsmith, sheriff, doctor, lookout,
        detective,
    ],
};

const neutralHidden = {
    name: 'Hidden Neutral',
    spawns: [jester, executioner, { role: blacksmith, faction: outcasts }],
};

const mafiaYakuzaFactionRoles = [...mafiaYakuzaHiddenSpawns, goon];

const schema = {
    description,
    enemies: [[town, mafia], [mafia, yakuza], [yakuza, town], [town, outcasts]],
    factions: [benigns, outcasts, town, mafia, yakuza],
    factionRoles: {
        [benigns.name]: [jester, executioner],
        [town.name]: [citizen, armorsmith, gunsmith, sheriff, doctor, lookout, detective, stripper],
        [mafia.name]: mafiaYakuzaFactionRoles,
        [yakuza.name]: mafiaYakuzaFactionRoles,
        [outcasts.name]: [blacksmith],
    },
    hiddens: [mafiaPowerRole, townPowerRole, yakuzaPowerRole, neutralHidden],
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: false,
    }, {
        name: setupModifiers.FOLLOW_GETS_ALL,
        value: false,
    }, {
        name: setupModifiers.BLOCK_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_SUCCESS_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.JESTER_CAN_ANNOY,
        value: false,
    }, {
        name: setupModifiers.JESTER_KILLS,
        value: 0,
    }, {
        name: setupModifiers.EXECUTIONER_TO_JESTER,
        value: true,
    }],
    name: 'Silence of the Lambs',
    roles: [
        stripper, armorsmith, gunsmith, doctor, detective, lookout, agent, blackmailer, framer,
        blacksmith, jester, executioner, goon, citizen, sheriff,
    ],
    setupHiddens: [
        citizen,
        citizen,
        citizen,
        townPowerRole,
        townPowerRole,
        mafiaPowerRole,
        getSetupHidden(mafiaPowerRole, 0, 7),
        getSetupHidden(neutralHidden, 8, 8),
        getSetupHidden({ faction: mafia, role: goon }, 8, 10),
        getSetupHidden(yakuzaPowerRole, 9),
        getSetupHidden({ faction: yakuza, role: goon }, 9, 10),
        getSetupHidden(citizen, 10),
        getSetupHidden(yakuzaPowerRole, 11, 11),
        getSetupHidden(mafiaPowerRole, 11),
        getSetupHidden(townPowerRole, 11),
        getSetupHidden({ faction: yakuza, role: goon }, 12),
        getSetupHidden({ faction: yakuza, role: goon }, 12, 14),
        getSetupHidden(citizen, 13),
        getSetupHidden(citizen, 14, 14),
        getSetupHidden({ faction: mafia, role: goon }, 15),
        getSetupHidden(yakuzaPowerRole, 15),
        getSetupHidden(townPowerRole, 15),
        getSetupHidden(neutralHidden, 16),
    ],
    sheriffDetectables: {
        [town.name]: [mafia, yakuza],
    },
    slogan,
    winPriority: [[yakuza, mafia], outcasts, town, benigns],
};

module.exports = {
    schema,
};
