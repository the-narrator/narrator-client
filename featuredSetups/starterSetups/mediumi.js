const { getSetupHidden } = require('../../util/playerCountHelpers');

const abilityModifiers = require('../../constants/abilityModifiers');
const factionModifiers = require('../../constants/factionModifiers');
const setupModifiers = require('../../constants/setupModifiers');

const {
    benigns, cult, town, threat: threatBase,
} = require('../definitions/baseFactions');

const {
    citizen, enforcer, bodyguard, stripper, sheriff, doctor, lookout, detective, mason, architect,
    jailor, jester, amnesiac,
} = require('../definitions/baseRoles');


const slogan = 'Basic Cult Game';
const description = 'New Town Roles - Mason, Clubber, Enforcer, Architect, Bodyguard, Jailor\n'
    + 'New Neutral Roles - Cultist, Cult Leader, Amnesiac\\n'
    + 'No mafia!';

const threat = {
    ...threatBase,
    modifiers: [...threatBase.modifiers, {
        name: factionModifiers.IS_RECRUITABLE,
        value: false,
    }],
};

const clubber = {
    name: 'Clubber',
    abilities: [{
        name: 'Clubber',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 1,
        }],
    }],
};

const serialKiller = {
    name: 'Serial Killer',
    abilities: [{
        name: 'SerialKiller',
        modifiers: [{
            name: abilityModifiers.COOLDOWN,
            value: 1,
            minPlayerCount: 0,
            maxPlayerCount: 12,
        }],
    }, {
        name: 'Bulletproof',
    }],
};

const cultLeader = {
    name: 'Cult Leader',
    abilities: [{
        name: 'CultLeader',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 2,
            minPlayerCount: 0,
            maxPlayerCount: 14,
        }, {
            name: abilityModifiers.CHARGES,
            value: 3,
            minPlayerCount: 15,
        }, {
            name: abilityModifiers.COOLDOWN,
            value: 1,
            minPlayerCount: 10,
            maxPlayerCount: 12,
        }],
    }],
};

const cultist = {
    name: 'Cultist',
    abilities: ['Cultist'],
};

const benignRandom = {
    name: 'Hidden Benign',
    spawns: [jester, amnesiac],
};

const townPowerRole = {
    name: 'Hidden Town',
    spawns: [
        stripper, sheriff, doctor, lookout, detective, mason, clubber, architect, jailor,
        { ...enforcer, minPlayerCount: 9 },
        { ...bodyguard, minPlayerCount: 10 },
    ],
};

const schema = {
    description,
    enemies: [[town, cult], [cult, threat], [threat, town]],
    factions: [threat, town, benigns, cult],
    factionRoles: {
        [town.name]: [
            citizen, enforcer, bodyguard, clubber, stripper, sheriff, doctor, lookout, detective,
            mason, architect, jailor,
        ],
        [benigns.name]: [amnesiac, jester],
        [threat.name]: [serialKiller],
        [cult.name]: [cultLeader, cultist],
    },
    hiddens: [townPowerRole, benignRandom],
    modifiers: [{
        name: setupModifiers.CHARGE_VARIABILITY,
        value: 0,
    }, {
        name: setupModifiers.AMNESIAC_MUST_REMEMBER,
        value: false,
    }, {
        name: setupModifiers.MASON_PROMOTION,
        value: false,
    }, {
        name: setupModifiers.ENFORCER_LEARNS_NAMES,
        value: false,
    }, {
        name: setupModifiers.DAY_START,
        value: false,
    }, {
        name: setupModifiers.FOLLOW_GETS_ALL,
        value: false,
    }, {
        name: setupModifiers.BLOCK_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_SUCCESS_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_FEEDBACK,
        value: false,
    }],
    name: 'Worshippers of Cthulu!',
    roles: [
        citizen, enforcer, bodyguard, cultLeader, clubber, stripper, sheriff, doctor, lookout,
        detective, mason, architect, jailor, amnesiac, jester, cultist, serialKiller,
    ],
    setupHiddens: [
        citizen,
        citizen,
        citizen,
        citizen,
        townPowerRole,
        townPowerRole,
        cultLeader,
        getSetupHidden(benignRandom, 8, 8),
        getSetupHidden(cultist, 9),
        getSetupHidden(townPowerRole, 9),
        getSetupHidden(serialKiller, 10),
        getSetupHidden(citizen, 11),
        getSetupHidden(benignRandom, 12),
        getSetupHidden(citizen, 13),
        getSetupHidden(citizen, 14, 14),
        getSetupHidden(townPowerRole, 15),
        getSetupHidden(benignRandom, 15),
        getSetupHidden(citizen, 16),
    ],
    sheriffDetectables: {
        [town.name]: [cult, threat],
    },
    slogan,
    winPriority: [threat, cult, town, benigns],
};

module.exports = {
    schema,
};
