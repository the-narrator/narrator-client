const abilityModifiers = require('../../constants/abilityModifiers');
const roleModifiers = require('../../constants/roleModifiers');


const agent = {
    abilities: ['Agent'],
    name: 'Agent',
};

const amnesiac = {
    abilities: ['Amnesiac'],
    name: 'Amnesiac',
};

const architect = {
    abilities: ['Architect'],
    name: 'Architect',
};

const armorsmith = {
    abilities: ['Armorsmith'],
    name: 'Armorsmith',
};

const armsDetector = {
    abilities: ['ArmsDetector'],
    name: 'Arms Detector',
};

const assassin = {
    abilities: ['Assassin'],
    name: 'Assassin',
};

const baker = {
    abilities: ['Baker'],
    name: 'Baker',
};

const blackmailer = {
    abilities: ['Blackmailer'],
    name: 'Blackmailer',
};

const bodyguard = {
    name: 'Bodyguard',
    abilities: ['Bodyguard'],
};

const bulletproof = {
    name: 'Bulletproof',
    abilities: ['Bulletproof'],
};

const bomb = {
    abilities: ['Bomb'],
    name: 'Bomb',
};

const busDriver = {
    name: 'Bus Driver',
    abilities: ['Driver'],
};

const citizen = {
    abilities: [],
    name: 'Citizen',
};

const clubber = {
    abilities: [{
        name: 'Clubber',
    }],
    name: 'Clubber',
};

const coroner = {
    abilities: ['Coroner'],
    name: 'Coroner',
};

const coward = {
    abilities: ['Coward'],
    name: 'Coward',
};

const detective = {
    abilities: ['Detective'],
    name: 'Detective',
};

const doctor = {
    abilities: ['Doctor'],
    name: 'Doctor',
};

const drugDealer = {
    abilities: ['DrugDealer'],
    name: 'Drug Dealer',
};

const elector = {
    abilities: ['Elector'],
    name: 'Elector',
};

const enforcer = {
    abilities: [{
        name: 'Enforcer',
    }],
    name: 'Enforcer',
};

const executioner = {
    abilities: [{
        name: 'Executioner',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 1,
        }],
    }],
    name: 'Executioner',
};

const framer = {
    abilities: ['Framer'],
    name: 'Framer',
};

const ghost = {
    name: 'Ghost',
    abilities: ['Ghost'],
};

const goon = {
    abilities: ['Goon'],
    name: 'Goon',
};

const graveDigger = {
    name: 'Grave Digger',
    abilities: ['GraveDigger'],
};

const gunsmith = {
    abilities: ['Gunsmith'],
    name: 'Gunsmith',
};

const interceptor = {
    name: 'Interceptor',
    abilities: ['Interceptor'],
};

const investigator = {
    abilities: ['Investigator'],
    name: 'Investigator',
};

const jailor = {
    abilities: ['JailExecute'],
    name: 'Jailor',
};

const janitor = {
    abilities: ['Janitor'],
    name: 'Janitor',
};

const jester = {
    abilities: ['Jester'],
    name: 'Jester',
};

const lookout = {
    abilities: ['Lookout'],
    name: 'Lookout',
};

const mason = {
    name: 'Mason',
    abilities: ['Mason'],
};

const mayor = {
    abilities: ['Mayor'],
    name: 'Mayor',
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }],
};

const miller = {
    abilities: ['Miller'],
    name: 'Miller',
};

const parityCop = {
    abilities: ['ParityCheck'],
    name: 'Parity Cop',
};

const operator = {
    abilities: ['Operator'],
    name: 'Operator',
};

const sheriff = {
    abilities: ['Sheriff'],
    name: 'Sheriff',
};

const silencer = {
    abilities: ['Silence'],
    name: 'Silencer',
};

const snitch = {
    abilities: ['Snitch'],
    name: 'Snitch',
};

const stripper = {
    abilities: ['Block'],
    name: 'Stripper',
};

const spy = {
    name: 'Spy',
    abilities: ['Spy'],
};

const thief = {
    abilities: ['Thief'],
    name: 'Thief',
};

const ventriloquist = {
    abilities: ['Ventriloquist'],
    name: 'Ventriloquist',
};

const vigilante = {
    abilities: ['Vigilante'],
    name: 'Vigilante',
};

const voteStopper = {
    abilities: ['Disfranchise'],
    name: 'Vote Stopper',
};

const witch = {
    abilities: ['Witch'],
    name: 'Witch',
};

module.exports = {
    agent,
    amnesiac,
    architect,
    armorsmith,
    armsDetector,
    assassin,
    baker,
    blackmailer,
    bomb,
    bodyguard,
    bulletproof,
    busDriver,
    citizen,
    clubber,
    coroner,
    coward,
    detective,
    doctor,
    drugDealer,
    elector,
    enforcer,
    executioner,
    framer,
    ghost,
    goon,
    graveDigger,
    gunsmith,
    investigator,
    interceptor,
    jailor,
    janitor,
    jester,
    lookout,
    mason,
    mayor,
    miller,
    parityCop,
    operator,
    sheriff,
    silencer,
    snitch,
    stripper,
    spy,
    thief,
    ventriloquist,
    vigilante,
    voteStopper,
    witch,
};
