const {
    blue, brown, pink, purple, red, sicklyGreen, yellow,
} = require('../../constants/colors');

const factionModifiers = require('../../constants/factionModifiers');


const benigns = {
    color: pink,
    description: "Their motto is 'I don't care who wins.'",
    name: 'Benigns',
};

const cult = {
    color: purple,
    description: 'A secretive organization, the cult looks to increase its ranks within the town',
    name: 'Cult',

    modifiers: [{
        name: factionModifiers.HAS_NIGHT_CHAT,
        value: true,
    }, {
        name: factionModifiers.KNOWS_ALLIES,
        value: true,
    }, {
        name: factionModifiers.IS_RECRUITABLE,
        value: false,
    }],
};

const mafia = {
    color: red,
    description: 'The informed minority, these roles must eliminate the Town, other Mafia '
        + 'factions, the cult and evil killing neutrals.',
    name: 'Mafia',

    abilities: [{
        name: 'FactionKill',
        modifiers: [],
    }],
    modifiers: [{
        name: factionModifiers.KNOWS_ALLIES,
        value: true,
    }, {
        name: factionModifiers.HAS_NIGHT_CHAT,
        value: true,
    }],
};

const outcasts = {
    color: sicklyGreen,
    description: 'Shunned by the general populace, they plot their revenge in hiding and look to '
        + 'ally themselves with anyone that shares their goal.',
    name: 'Outcasts',
    modifiers: [{
        name: factionModifiers.LIVE_TO_WIN,
        value: true,
    }],
};

const threat = {
    color: brown,
    description: "Their motto is 'I don't care who wins.'",
    name: 'Threat',
    modifiers: [{
        name: factionModifiers.LIVE_TO_WIN,
        value: true,
    }],
};

const town = {
    color: blue,
    description: 'The uninformed majority, these roles must eliminate all Mafia factions, '
        + 'the cult, and all evil neutrals.',
    name: 'Town',
};

const yakuza = {
    ...mafia,
    color: yellow,
    name: 'Yakuza',
};

module.exports = {
    benigns,
    cult,
    mafia,
    outcasts,
    town,
    threat,
    yakuza,
};
