const { getSetupHidden } = require('../util/playerCountHelpers');

const abilityModifiers = require('../constants/abilityModifiers');
const setupModifiers = require('../constants/setupModifiers');

const { town, mafia: mafiaBase } = require('./definitions/baseFactions');

const {
    architect, assassin, baker, busDriver, doctor, drugDealer, framer, janitor, investigator,
    operator, stripper, witch, citizen, armorsmith, armsDetector, bodyguard, bulletproof, coroner,
    detective, gunsmith, lookout, mason, mayor, parityCop, sheriff, snitch, spy, agent, graveDigger,
} = require('./definitions/baseRoles');


const slogan = 'All roles enabled! (MU Style)';

const mafia = {
    ...mafiaBase,
    abilities: [{
        name: 'FactionKill',
        modifiers: [{
            name: abilityModifiers.ZERO_WEIGHTED,
            value: true,
        }],
    }],
};

const blacksmith = {
    name: 'Blacksmith',
    abilities: [{
        name: 'Blacksmith',
        modifiers: [{
            name: abilityModifiers.AS_FAKE_VESTS,
            value: true,
        }, {
            name: abilityModifiers.GS_FAULTY_GUNS,
            value: true,
        }],
    }],
};

const coward = {
    name: 'Coward',
    abilities: [{
        name: 'Coward',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const disguiser = {
    name: 'Disguiser',
    abilities: [{
        name: 'Disguiser',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 1,
        }],
    }],
};

const electromaniac = {
    name: 'Electro Maniac',
    abilities: [{
        name: 'ElectroManiac',
        modifiers: [],
    }],
};

const jailor = {
    name: 'Jailor',
    abilities: [{
        name: 'JailExecute',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 2,
        }],
    }],
};

const poisoner = {
    name: 'Poisoner',
    abilities: ['Poisoner'],
};

const sleepwalker = {
    name: 'Sleepwalker',
    abilities: ['Sleepwalker'],
    receivedFactionRole: citizen,
};

const tailor = {
    name: 'Tailor',
    abilities: [{
        name: 'Tailor',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const vigilante = {
    name: 'Vigilante',
    abilities: [{
        name: 'Vigilante',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 2,
        }],
    }],
};

const veteran = {
    name: 'Veteran',
    abilities: [{
        name: 'Veteran',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const mafiaRandom = {
    name: 'Hidden Mafia',
    spawns: [
        blacksmith, disguiser, agent, graveDigger, architect, baker, coward, drugDealer,
        electromaniac, investigator, janitor, poisoner, tailor, witch, stripper, operator, jailor,
        framer, doctor, busDriver, assassin,
    ].map(spawn => ({ ...spawn, role: spawn, faction: mafia })),
};

const townRandom = {
    name: 'Hidden Town',
    spawns: [
        citizen, veteran, vigilante, armorsmith, armsDetector, bodyguard, bulletproof, coroner,
        detective, gunsmith, lookout, mason, mayor, parityCop, sheriff, sleepwalker, snitch,
        spy, architect, baker, coward, drugDealer, electromaniac, investigator, janitor,
        tailor,
    ],
};

const schema = {
    description: slogan,
    enemies: [[mafia, town]],
    factions: [town, mafia],
    factionRoles: {
        [mafia.name]: mafiaRandom.spawns,
        [town.name]: townRandom.spawns,
    },
    featuredKey: 'mumash',
    featuredPriority: 10,
    hiddens: [mafiaRandom, townRandom],
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: true,
    }, {
        name: setupModifiers.FOLLOW_GETS_ALL,
        value: false,
    }, {
        name: setupModifiers.CIT_RATIO,
        value: 40,
    }],
    name: 'MU Mash',
    roles: [
        agent,
        architect,
        armorsmith,
        armsDetector,
        assassin,
        baker,
        blacksmith,
        bodyguard,
        bulletproof,
        busDriver,
        citizen,
        coroner,
        coward,
        detective,
        disguiser,
        doctor,
        drugDealer,
        electromaniac,
        framer,
        graveDigger,
        gunsmith,
        jailor,
        janitor,
        investigator,
        lookout,
        mason,
        mayor,
        operator,
        parityCop,
        poisoner,
        sheriff,
        sleepwalker,
        snitch,
        spy,
        stripper,
        tailor,
        veteran,
        vigilante,
        witch,
    ],
    setupHiddens: [
        mafiaRandom,
        townRandom,
        townRandom,
        townRandom,
        getSetupHidden(townRandom, 5),
        getSetupHidden(townRandom, 6),
        getSetupHidden(mafiaRandom, 7),
        getSetupHidden(townRandom, 8),
        getSetupHidden(mafiaRandom, 9),
        getSetupHidden(townRandom, 10),
        getSetupHidden(townRandom, 11),
        getSetupHidden(townRandom, 12),
        getSetupHidden(townRandom, 13),
        getSetupHidden(townRandom, 14),
        getSetupHidden(mafiaRandom, 15),
        getSetupHidden(townRandom, 16),
    ],
    sheriffDetectables: {
        [town.name]: [mafia],
    },
    slogan,
    winPriority: [mafia, town],
};

module.exports = {
    schema,
};
