const { getSetupHidden } = require('../util/playerCountHelpers');

const setupModifiers = require('../constants/setupModifiers');

const { town, mafia } = require('./definitions/baseFactions');

const {
    citizen, doctor, goon, sheriff,
} = require('./definitions/baseRoles');


const slogan = 'The classic game of Mafia you know and love';

const schema = {
    description: slogan,
    enemies: [[town, mafia]],
    factions: [town, mafia],
    factionRoles: {
        [town.name]: [citizen, doctor, sheriff],
        [mafia.name]: [goon],
    },
    featuredKey: 'classic',
    featuredPriority: 1,
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: false,
    }, {
        name: setupModifiers.HEAL_SUCCESS_FEEDBACK,
        value: true,
    }, {
        name: setupModifiers.HEAL_FEEDBACK,
        value: true,
    }],
    name: 'Classic',
    roles: [citizen, doctor, goon, sheriff],
    setupHiddens: [
        citizen,
        citizen,
        citizen,
        sheriff,
        doctor,
        goon,
        goon,
        getSetupHidden(citizen, 8),
        getSetupHidden(citizen, 9),
        getSetupHidden(goon, 10),
        getSetupHidden(citizen, 11),
        getSetupHidden(citizen, 12),
        getSetupHidden(goon, 13),
        getSetupHidden(citizen, 14, 14),
        getSetupHidden(goon, 15),
        getSetupHidden(sheriff, 15),
        getSetupHidden(citizen, 16),
    ],
    sheriffDetectables: {
        [town.name]: [mafia],
    },
    slogan,
    winPriority: [mafia, town],
};

module.exports = {
    schema,
};
