const setupModifiers = require('../constants/setupModifiers');

const { town, mafia } = require('./definitions/baseFactions');

const { citizen, goon, mayor } = require('./definitions/baseRoles');


const schema = {
    description: 'Small 5-player mafia',
    enemies: [[town, mafia]],
    factions: [town, mafia],
    factionRoles: {
        [town.name]: [citizen, mayor],
        [mafia.name]: [goon],
    },
    featuredKey: 'fivep',
    featuredPriority: 12,
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: true,
    }, {
        name: setupModifiers.MAYOR_VOTE_POWER,
        value: 0,
    }],
    name: '5 Player Mafia',
    roles: [citizen, goon, mayor],
    setupHiddens: [citizen, citizen, citizen, goon, mayor],
    slogan: 'Small 5-player mafia',
    winPriority: [mafia, town],
};

module.exports = {
    schema,
};
