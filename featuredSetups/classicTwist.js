const { getSetupHidden } = require('../util/playerCountHelpers');

const setupModifiers = require('../constants/setupModifiers');

const { town, mafia } = require('./definitions/baseFactions');

const {
    bomb, citizen, doctor, goon, gunsmith, sheriff, snitch,
} = require('./definitions/baseRoles');


const slogan = 'Epic Mafia\'s classic rotational setup';
const description = 'Simple Cop and random power role\nPower Role : Gunsmith, Bomb, Doctor, Snitch';

const townPowerRole = {
    name: 'Power Role',
    spawns: [bomb, doctor, gunsmith, snitch],
};

const schema = {
    description,
    enemies: [[town, mafia]],
    factions: [town, mafia],
    factionRoles: {
        [town.name]: [citizen, doctor, sheriff, bomb, gunsmith, snitch],
        [mafia.name]: [goon],
    },
    featuredKey: 'classicplus',
    featuredPriority: 11,
    hiddens: [townPowerRole],
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: false,
    }, {
        name: setupModifiers.HEAL_SUCCESS_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.GS_DAY_GUNS,
        value: true,
    }],
    name: 'Classic Twist',
    roles: [bomb, citizen, doctor, goon, gunsmith, sheriff, snitch],
    setupHiddens: [
        citizen,
        citizen,
        citizen,
        sheriff,
        townPowerRole,
        goon,
        goon,
        getSetupHidden(citizen, 8),
        getSetupHidden(citizen, 9),
        getSetupHidden(goon, 10),
        getSetupHidden(townPowerRole, 11),
        getSetupHidden(citizen, 12),
        getSetupHidden(citizen, 13),
        getSetupHidden(citizen, 14, 14),
        getSetupHidden(goon, 15),
        getSetupHidden(sheriff, 15),
        getSetupHidden(citizen, 16),
    ],
    sheriffDetectables: {
        [town.name]: [mafia],
    },
    slogan,
    winPriority: [mafia, town],
};

module.exports = {
    schema,
};
