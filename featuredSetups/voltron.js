const roleModifiers = require('../constants/roleModifiers');
const setupModifiers = require('../constants/setupModifiers');

const { town, mafia } = require('./definitions/baseFactions');
const {
    citizen, goon, snitch, miller, investigator,
} = require('./definitions/baseRoles');


const bulletproof = {
    name: 'Bulletproof',
    abilities: [],
    modifiers: [
        { name: roleModifiers.AUTO_VEST, value: 1 },
    ],
};
const oracle = { ...snitch, name: 'Oracle' };

const factionKill = mafia.abilities.find(ability => ability.name === 'FactionKill');
factionKill.modifiers.push({
    name: 'ZERO_WEIGHTED',
    value: true,
});

const schema = {
    description: '8 Player Oracle/BP Game',
    enemies: [[town, mafia]],
    factions: [town, mafia],
    factionRoles: {
        [town.name]: [citizen, { ...bulletproof, receivedFactionRole: citizen }, oracle, miller],
        [mafia.name]: [investigator, goon],
    },
    featuredKey: 'voltron',
    featuredPriority: 15,
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: false,
    }, {
        name: setupModifiers.MILLER_SUITED,
        value: true,
    }],
    name: '8 Player Oracle/BP Game',
    roles: [oracle, citizen, miller, bulletproof, investigator, goon],
    setupHiddens: [citizen, citizen, citizen, miller, oracle, bulletproof, goon, investigator],
    slogan: '8 Player Oracle/BP Game',
    winPriority: [mafia, town],
};

module.exports = {
    schema,
};
