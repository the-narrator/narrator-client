const setupModifiers = require('../constants/setupModifiers');

const { town, mafia } = require('./definitions/baseFactions');

const { citizen, sheriff, goon } = require('./definitions/baseRoles');


const schema = {
    description: 'Cop knows an ally at the start of the game',
    enemies: [[town, mafia]],
    factions: [town, mafia],
    factionRoles: {
        [town.name]: [citizen, sheriff],
        [mafia.name]: [goon],
    },
    featuredKey: 'cop9er',
    featuredPriority: 14,
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: true,
    }, {
        name: setupModifiers.SHERIFF_PREPEEK,
        value: true,
    }],
    name: 'Cop9er',
    roles: [goon, citizen, sheriff],
    setupHiddens: [goon, goon, sheriff, citizen, citizen, citizen, citizen, citizen, citizen],
    sheriffDetectables: {
        [town.name]: [mafia],
    },
    slogan: '9 Player Cop Game',
    winPriority: [mafia, town],
};

module.exports = {
    schema,
};
