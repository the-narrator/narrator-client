const setupModifiers = require('../constants/setupModifiers');

const { town, mafia } = require('./definitions/baseFactions');
const { sheriff, goon, framer } = require('./definitions/baseRoles');


const factionKill = mafia.abilities.find(ability => ability.name === 'FactionKill');
factionKill.modifiers.push({
    name: 'ZERO_WEIGHTED',
    value: true,
});

const miller = {
    name: 'Miller',
    abilities: ['Miller'],
};

const schema = {
    description: '7 Player Cop/Miller Game',
    enemies: [[town, mafia]],
    factions: [town, mafia],
    factionRoles: {
        [town.name]: [sheriff, miller],
        [mafia.name]: [goon, framer],
    },
    featuredKey: 'millercop7',
    featuredPriority: 13,
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: false,
    }, {
        name: setupModifiers.MILLER_SUITED,
        value: true,
    }],
    name: '7 Player Cop/Miller Game',
    roles: [sheriff, miller, goon, framer],
    setupHiddens: [sheriff, sheriff, sheriff, sheriff, miller, framer, goon],
    sheriffDetectables: {
        [town.name]: [mafia],
    },
    slogan: '7 Player Cop/Miller Game',
    winPriority: [mafia, town],
};

module.exports = {
    schema,
};
