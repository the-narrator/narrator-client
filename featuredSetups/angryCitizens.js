const { town } = require('./definitions/baseFactions');

const { citizen } = require('./definitions/baseRoles');

function getColor(color, offset){
    const decimal = parseInt(color.substring(1), 16) + offset;
    return `#${decimal.toString(16)}`;
}

const factions = [];
const enemies = [];
const factionRoles = {};
const setupHiddens = [];
for(let i = 0; i < 25; i++){
    factions.push({
        ...town,
        name: `Town${i + 1}`,
        color: getColor(town.color, i),
    });
    factionRoles[factions[i].name] = [citizen];
    setupHiddens.push({ faction: factions[i], role: citizen });
    for(let j = 0; j < i; j++)
        enemies.push([factions[i], factions[j]]);
}

const slogan = 'Eliminate everyone else.';

const schema = {
    description: slogan,
    enemies,
    factions,
    factionRoles,
    featuredKey: 'angry',
    featuredPriority: 30,
    modifiers: [],
    name: 'Angry Citizens',
    roles: [citizen],
    setupHiddens,
    sheriffDetectables: {},
    slogan,
    winPriority: [],
};

module.exports = {
    schema,
};
