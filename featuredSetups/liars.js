const { getSetupHidden } = require('../util/playerCountHelpers');

const abilityModifiers = require('../constants/abilityModifiers');
const roleModifiers = require('../constants/roleModifiers');
const setupModifiers = require('../constants/setupModifiers');

const {
    citizen, doctor, goon, thief,
} = require('./definitions/baseRoles');

const {
    town, mafia, outcasts, benigns,
} = require('./definitions/baseFactions');


const slogan = 'Liar\'s Club setup';

const alien = {
    name: 'Alien',
    abilities: [],
};

const axe = {
    name: 'Axe',
    abilities: ['Silence', {
        name: 'Disfranchise',
        modifiers: [{
            name: abilityModifiers.SILENCE_ANNOUNCED,
            value: true,
        }, {
            name: abilityModifiers.DISFRANCHISED_ANNOUNCED,
            value: true,
        }],
    }],
};

const cop = {
    name: 'Cop',
    abilities: ['Sheriff'],
};

const prostitute = {
    name: 'Prostitute',
    abilities: ['Block'],
    modifiers: [{
        name: roleModifiers.UNDETECTABLE,
        value: true,
    }],
};

const schema = {
    description: slogan,
    enemies: [[town, mafia], [town, outcasts]],
    factions: [town, mafia, outcasts, benigns],
    factionRoles: {
        [benigns.name]: [alien, thief],
        [mafia.name]: [axe, goon],
        [outcasts.name]: [prostitute],
        [town.name]: [citizen, cop, doctor],
    },
    // featuredKey: 'liars',
    // featuredPriority: 16,
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: true,
    }, {
        name: setupModifiers.SKIP_VOTE,
        value: false,
    }, {
        name: setupModifiers.LAST_WILL,
        value: false,
    }, {
        name: setupModifiers.SELF_VOTE,
        value: true,
    }, {
        name: setupModifiers.SECRET_VOTES,
        value: true,
    }],
    name: 'Liars Club setup',
    roles: [
        axe, alien, citizen, cop, doctor, goon, prostitute, thief,
    ].map(r => ({
        ...r,
        modifiers: [
            ...(r.modifiers || []),
            { name: roleModifiers.HIDDEN_FLIP, value: true },
        ],
    })),
    setupHiddens: [
        citizen,
        citizen,
        citizen,
        citizen,
        doctor,
        cop,
        prostitute,
        goon,
        getSetupHidden(goon, 0, 9),
        thief,
        alien,
        getSetupHidden(axe, 10),
        getSetupHidden(citizen, 10),
        getSetupHidden(citizen, 11),
        getSetupHidden(citizen, 12),
        getSetupHidden(citizen, 13),
    ],
    sheriffDetectables: {
        [town.name]: [mafia],
    },
    slogan,
    winPriority: [[mafia, outcasts], [town, benigns]],
};

module.exports = {
    schema,
};
