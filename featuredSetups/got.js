const {
    red, orange, yellow, green, blue, purple,
} = require('../constants/colors');

const { getSetupHidden } = require('../util/playerCountHelpers');

const factionModifier = require('../constants/factionModifiers');
const abilityModifiers = require('../constants/abilityModifiers');
const roleModifiers = require('../constants/roleModifiers');
const setupModifiers = require('../constants/setupModifiers');

const {
    architect, witch, jailor, agent, assassin, framer, investigator,
    bodyguard, doctor, sheriff, detective: detectiveBase,
    citizen, baker, coroner, mason, mayor, snitch,
} = require('./definitions/baseRoles');


const slogan = 'The epic 6 sided version of Mafia';

const martells = {
    name: 'Martell',
    color: red,
    description: 'Unbowed, Unbent, Unbroken',
    modifiers: [{
        name: factionModifier.KNOWS_ALLIES,
        value: true,
    }],
};

const mafiaFactionAttributes = {
    modifiers: [{
        name: factionModifier.KNOWS_ALLIES,
        value: true,
    }, {
        name: factionModifier.HAS_NIGHT_CHAT,
        value: true,
    }],
    abilities: [{
        name: 'FactionKill',
        modifiers: [],
    }],
};

const neutralFactionAttributes = {
    modifiers: [{
        name: factionModifier.LAST_WILL,
        value: true,
    }],
};

const baratheons = {
    name: 'Baratheon',
    color: purple,
    description: 'Ours is the Fury',
    ...mafiaFactionAttributes,
};

const lannisters = {
    name: 'Lannister',
    color: orange,
    description: 'A Lannister always pays his debts',
    ...mafiaFactionAttributes,
};

const tyrells = {
    name: 'Tyrell',
    color: yellow,
    description: 'Growing strong',
    ...neutralFactionAttributes,
};

const starks = {
    name: 'Stark',
    color: blue,
    description: 'Winter is coming',
    ...neutralFactionAttributes,
};

const targaryens = {
    name: 'Targaryen',
    color: green,
    description: 'Fire and Blood',
};

const modifiers = [{
    name: setupModifiers.CHARGE_VARIABILITY,
    value: 2,
}, {
    name: setupModifiers.DIFFERENTIATED_FACTION_KILLS,
    value: false,
}, {
    name: setupModifiers.DAY_START,
    value: true,
}, {
    name: setupModifiers.SPY_TARGETS_ALLIES,
    value: false,
}, {
    name: setupModifiers.SPY_TARGETS_ENEMIES,
    value: true,
}, {
    name: setupModifiers.GS_DAY_GUNS,
    value: false,
}, {
    name: setupModifiers.BREAD_PASSING,
    value: false,
}, {
    name: setupModifiers.SELF_BREAD_USAGE,
    value: false,
}, {
    name: setupModifiers.CHECK_DIFFERENTIATION,
    value: false,
}, {
    name: setupModifiers.CORONER_EXHUMES,
    value: false,
}, {
    name: setupModifiers.FOLLOW_GETS_ALL,
    value: false,
}, {
    name: setupModifiers.HEAL_SUCCESS_FEEDBACK,
    value: true,
}, {
    name: setupModifiers.HEAL_FEEDBACK,
    value: true,
}, {
    name: setupModifiers.HEAL_BLOCKS_POISON,
    value: false,
}, {
    name: setupModifiers.CIT_RATIO,
    value: 40,
}, {
    name: setupModifiers.ARCH_QUARANTINE,
    value: false,
}, {
    name: setupModifiers.MASON_PROMOTION,
    value: false,
}, {
    name: setupModifiers.MAYOR_VOTE_POWER,
    value: 3,
}, {
    name: setupModifiers.GD_REANIMATE,
    value: true,
}, {
    name: setupModifiers.ARSON_DAY_IGNITES,
    value: -1,
}, {
    name: setupModifiers.JANITOR_GETS_ROLES,
    value: false,
}, {
    name: setupModifiers.TAILOR_FEEDBACK,
    value: false,
}, {
    name: setupModifiers.SNITCH_PIERCES_SUIT,
    value: true,
}, {
    name: setupModifiers.GUN_NAME,
    value: 'sword',
}, {
    name: setupModifiers.VEST_NAME,
    value: 'armor',
}];

const enemies = [[targaryens, martells], [targaryens, lannisters], [targaryens, starks],
    [starks, martells], [starks, lannisters], [starks, tyrells],
    [lannisters, baratheons], [baratheons, tyrells], [martells, tyrells]];

const alchemist = {
    name: 'Alchemist',
    abilities: ['DrugDealer'],
};

const blacksmith = {
    name: 'Arms Dealer',
    abilities: ['Blacksmith'],
};

const strategist = {
    name: 'Strategist',
    abilities: ['Operator'],
};

const wagonDriver = {
    name: 'Wagon Driver',
    abilities: ['Driver'],
};

const whore = {
    name: 'Whore',
    abilities: ['Block'],
};

const warg = {
    name: 'Warg',
    abilities: ['Ventriloquist'],
};

const beserker = {
    name: 'Berserker',
    abilities: ['MassMurderer'],
};

const necromancer = {
    name: 'Necromancer',
    abilities: ['GraveDigger'],
};

const poisoner = {
    name: 'Poisoner',
    abilities: ['Poisoner'],
};

const serialKiller = {
    name: 'Serial Killer',
    abilities: ['SerialKiller'],
};

const survivor = {
    name: 'Survivor',
    abilities: ['Survivor'],
};

const firePriestess = {
    name: 'Fire Priestess',
    abilities: ['Douse', 'Undouse', 'Burn'],
};

const martellBlacksmith = {
    ...blacksmith,
    abilityModifiers: {
        Blacksmith: [{
            name: abilityModifiers.AS_FAKE_VESTS,
            value: true,
        }, {
            name: abilityModifiers.GS_FAULTY_GUNS,
            value: true,
        }],
    },
};

const martellSurvivor = {
    ...survivor,
    roleModifiers: [{
        name: roleModifiers.AUTO_VEST,
        value: 50,
    }],
};

const townSurvivor = {
    ...survivor,
    abilityModifiers: {
        Survivor: [{
            name: abilityModifiers.CHARGES,
            value: 4,
        }],
    },
};

const nonKillingMartells = [
    alchemist, architect, necromancer, strategist, martellSurvivor, wagonDriver, warg, witch,
    whore,
];

const killingMartells = [
    beserker, firePriestess, poisoner, serialKiller,
];

function getMartellLeader(role){
    return {
        name: `${role.name} Leader`,
        abilities: [...role.abilities, 'Bulletproof'],
        roleModifiers: [
            ...(role.roleModifiers || []),
            {
                name: roleModifiers.UNBLOCKABLE,
                value: true,
            },
        ],
    };
}

const nonKillingMartellLeaders = nonKillingMartells.map(getMartellLeader);
const killingMartellLeaders = killingMartells.map(getMartellLeader);

const martellAllLeaderHidden = {
    name: 'Martell Unknown',
    spawns: [...nonKillingMartellLeaders, ...killingMartellLeaders],
};

const martellNonkillingLeaderHidden = {
    name: 'Martell Leader',
    spawns: nonKillingMartellLeaders,
};

const martellKilling = {
    name: 'Martell Killer',
    spawns: killingMartells,
};

const martellNonKiller = {
    name: 'Martell Member',
    spawns: nonKillingMartells.map(role => ({ role, faction: martells })),
};

const coward = {
    name: 'Coward',
    abilities: [{
        name: 'Coward',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 4,
        }],
    }],
};

const disguiser = {
    name: 'Disguiser',
    abilities: [{
        name: 'Disguiser',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 1,
        }],
    }],
};

const janitor = {
    name: 'Janitor',
    abilities: [{
        name: 'Janitor',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const spy = {
    name: 'Spy',
    abilities: [{
        name: 'Spy',
    }],
};

const mafiaSpy = {
    ...spy,
    abilityModifiers: {
        Spy: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    },
};

const tailor = {
    name: 'Tailor',
    abilities: [{
        name: 'Tailor',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 2,
        }],
    }],
};

const mafiaRoles = [
    agent, assassin, alchemist, coward, disguiser, framer, jailor, janitor,
    investigator, mafiaSpy, strategist, tailor, warg, wagonDriver, whore,
];

const lannisterHidden = {
    name: 'Lannister Member',
    spawns: mafiaRoles.map(role => ({ role, faction: lannisters })),
};

const baratheonHidden = {
    name: 'Baratheon Member',
    spawns: mafiaRoles.map(role => ({ role, faction: baratheons })),
};

function getMafiaLeader(role){
    return {
        name: `${role.name} Head`,
        abilities: role.abilities,
        roleModifiers: [
            ...(role.roleModifiers || []),
            {
                name: roleModifiers.UNDETECTABLE,
                value: true,
            }, {
                name: roleModifiers.AUTO_VEST,
                value: 1,
                minPlayerCount: 0,
                maxPlayerCount: 27,
            }, {
                name: roleModifiers.AUTO_VEST,
                value: 50,
                minPlayerCount: 28,
            },
        ],
    };
}

const mafiaLeaderRoles = mafiaRoles.map(getMafiaLeader);

const baratheonLeader = {
    name: 'Baratheon Leader',
    spawns: mafiaLeaderRoles.map(role => ({ role, faction: baratheons })),
};

const lannisterLeader = {
    name: 'Lannister Leader',
    spawns: mafiaLeaderRoles.map(role => ({ role, faction: lannisters })),
};

const archer = {
    name: 'Archer',
    abilities: [{
        name: 'Vigilante',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const hunter = {
    ...detectiveBase,
    name: 'Hunter',
};

const neutralSpy = {
    ...spy,
    abilityModifiers: {
        Spy: [{
            name: abilityModifiers.CHARGES,
            value: 2,
        }],
    },
};

const watchman = {
    name: 'Watchman',
    abilities: ['Lookout'],
};

const neutralRoles = [bodyguard, doctor, archer, sheriff, hunter, neutralSpy, watchman, jailor,
    strategist, whore, wagonDriver];

const starkMembers = {
    name: 'Stark Member',
    spawns: neutralRoles.map(role => ({ role, faction: starks })),
};

const tyrellMembers = {
    name: 'Tyrell Member',
    spawns: neutralRoles.map(role => ({ role, faction: tyrells })),
};

const townArchitect = {
    ...architect,
    abilityModifiers: {
        Architect: [{
            name: abilityModifiers.SELF_TARGET,
            value: false,
        }],
    },
};

const drunk = {
    name: 'Drunk',
    abilities: ['Amnesiac'],
};

const townCoward = {
    ...coward,
    abilityModifiers: {
        Coward: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    },
};

const knight = {
    name: 'Knight',
    abilities: [{
        name: 'Veteran',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const nurse = {
    name: 'Nurse',
    abilities: [{
        name: 'Block',
        modifiers: [{
            name: abilityModifiers.COOLDOWN,
            value: 1,
        }],
    }, {
        name: 'Doctor',
        modifiers: [{
            name: abilityModifiers.COOLDOWN,
            value: 1,
        }],
    }],
};

const sleepwalker = {
    name: 'Sleepwalker',
    abilities: ['Sleepwalker'],
    receivedFactionRole: citizen,
};

const townRoles = [
    drunk, townSurvivor, townArchitect, citizen, townCoward, knight, sleepwalker,
    archer, baker, bodyguard, coroner, doctor, hunter, mason, mayor, nurse, snitch,
    watchman, wagonDriver, whore, sheriff, blacksmith,
];

const targaryenMembers = {
    name: 'Targaryen Member',
    spawns: townRoles.map(role => ({ role, faction: targaryens })),
};

const schema = {
    description: slogan,
    enemies,
    factions: [martells, targaryens, baratheons, lannisters, tyrells, starks],
    factionRoles: {
        [lannisters.name]: [...mafiaRoles, ...mafiaLeaderRoles],
        [baratheons.name]: [...mafiaRoles, ...mafiaLeaderRoles],
        [martells.name]: [martellBlacksmith, ...nonKillingMartells, ...killingMartells,
            ...nonKillingMartellLeaders, ...killingMartellLeaders],
        [targaryens.name]: townRoles,
        [starks.name]: neutralRoles,
        [tyrells.name]: neutralRoles,
    },
    featuredPriority: 17,
    featuredKey: 'got',
    hiddens: [
        starkMembers, tyrellMembers, targaryenMembers,
        lannisterHidden, baratheonHidden, lannisterLeader, baratheonLeader,
        martellAllLeaderHidden, martellNonkillingLeaderHidden, martellKilling, martellNonKiller,
    ],
    modifiers,
    name: 'Game of Thrones',
    roles: [
        baker, coroner, mason, mayor, snitch,
        drunk, nurse, sleepwalker, knight, citizen,
        bodyguard, doctor, archer, sheriff, hunter, watchman,
        ...mafiaLeaderRoles,
        agent, assassin, framer, investigator,
        coward, jailor, disguiser, janitor, spy, tailor,
        alchemist, architect, blacksmith, strategist, wagonDriver, warg, whore, witch,
        beserker, necromancer, poisoner, serialKiller, survivor, firePriestess,
        ...nonKillingMartellLeaders, ...killingMartellLeaders,
    ],
    setupHiddens: [
        targaryenMembers,
        targaryenMembers,
        targaryenMembers,
        targaryenMembers,
        targaryenMembers,
        targaryenMembers,
        targaryenMembers,
        starkMembers,
        starkMembers,
        tyrellMembers,
        tyrellMembers,
        lannisterLeader,
        lannisterHidden,
        baratheonLeader,
        baratheonHidden,
        getSetupHidden(martellNonkillingLeaderHidden, 0, 21),
        getSetupHidden(targaryenMembers, 17, 17),
        getSetupHidden(tyrellMembers, 18),
        getSetupHidden(baratheonHidden, 18),
        getSetupHidden(targaryenMembers, 19),
        getSetupHidden(targaryenMembers, 20, 20),
        getSetupHidden(starkMembers, 21),
        getSetupHidden(lannisterHidden, 21),
        getSetupHidden(martellAllLeaderHidden, 22, 22),
        getSetupHidden(targaryenMembers, 22),
        getSetupHidden(martellNonkillingLeaderHidden, 23),
        getSetupHidden(martellKilling, 23),
        getSetupHidden(targaryenMembers, 24, 24),
        getSetupHidden(tyrellMembers, 25),
        getSetupHidden(starkMembers, 25),
        getSetupHidden(targaryenMembers, 26),
        getSetupHidden(martellNonKiller, 27),
        getSetupHidden(targaryenMembers, 28),
    ],
    sheriffDetectables: {
        [targaryens.name]: [baratheons, lannisters, martells],
        [starks.name]: [lannisters, tyrells, martells],
        [tyrells.name]: [baratheons, martells, starks],
    },
    slogan,
    winPriority: [martells, [baratheons, lannisters], [tyrells, starks, targaryens]],
};

module.exports = {
    schema,
};
