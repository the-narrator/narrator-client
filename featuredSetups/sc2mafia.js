const { getSetupHidden } = require('../util/playerCountHelpers');

const abilityModifiers = require('../constants/abilityModifiers');
const roleModifiers = require('../constants/roleModifiers');
const setupModifiers = require('../constants/setupModifiers');

const {
    agent, amnesiac, armorsmith, blackmailer, bodyguard, busDriver, citizen, coroner, detective,
    doctor, elector, executioner, framer, goon, investigator, jester, janitor, lookout, mason,
    mayor, sheriff, spy, stripper, witch,
} = require('./definitions/baseRoles');

const {
    benigns, town, outcasts, cult, mafia, threat,
} = require('./definitions/baseFactions');


const slogan = 'Typical setup on SC2 mod';

const factionKill = mafia.abilities.find(ability => ability.name === 'FactionKill');
factionKill.modifiers.push({
    name: 'ZERO_WEIGHTED',
    value: true,
});

const arsonist = {
    name: 'Arsonist',
    abilities: ['Burn', 'Douse', 'Undouse', 'Bulletproof'],
};

const coward = {
    name: 'Coward',
    abilities: [{
        name: 'Coward',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const cultist = {
    name: 'Cultist',
    abilities: ['Cultist'],
};

const cultLeader = {
    name: 'Cult Leader',
    abilities: ['CultLeader'],
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }],
};

const disguiser = {
    name: 'Disguiser',
    abilities: [{
        name: 'Disguiser',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 1,
        }],
    }],
};

const godfather = {
    name: 'Godfather',
    abilities: ['Bulletproof', 'Godfather'],
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }, {
        name: roleModifiers.UNDETECTABLE,
        value: true,
    }],
};

const jailor = {
    name: 'Jailor',
    abilities: [{
        name: 'JailExecute',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 2,
        }],
    }],
};

const marshall = {
    name: 'Marshall',
    abilities: [{
        name: 'Marshall',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 2,
        }],
    }],
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }],
};

const masonLeader = {
    name: 'Mason Leader',
    abilities: [{
        name: 'MasonLeader',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 4,
        }],
    }],
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }],
};

const massMurderer = {
    name: 'Mass Murderer',
    abilities: ['MassMurderer', 'Bulletproof'],
};

const poisoner = {
    name: 'Poisoner',
    abilities: ['Poisoner'],
};

const serialKiller = {
    name: 'Serial Killer',
    abilities: ['SerialKiller', 'Bulletproof'],
};

const snitch = {
    name: 'Snitch',
    abilities: ['Snitch'],
    modifiers: [{
        name: roleModifiers.UNBLOCKABLE,
        value: true,
    }],
};

const survivor = {
    name: 'Survivor',
    abilities: [{
        name: 'Survivor',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 4,
        }],
    }],
};

const veteran = {
    name: 'Veteran',
    abilities: [{
        name: 'Veteran',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const vigilante = {
    name: 'Vigilante',
    abilities: [{
        name: 'Vigilante',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const benignsRandom = {
    name: 'Benign Random',
    spawns: [amnesiac, executioner, jester, survivor],
};

const mafiaRandom = {
    name: 'Mafia Random',
    spawns: [
        agent, blackmailer, coward, disguiser, framer, goon, investigator,
        { role: stripper, faction: mafia }, janitor, { role: stripper, faction: mafia },
    ],
};

const neutralEvilRandom = {
    name: 'Neutral Evil',
    spawns: [
        cultLeader, cultist, elector, witch,
    ],
};

const threatRandom = {
    name: 'Neutral Killer',
    spawns: [arsonist, massMurderer, poisoner, serialKiller],
};

const townGovernment = {
    name: 'Town Government',
    spawns: [mayor, marshall, masonLeader, mason],
};

const townInvestigative = {
    name: 'Town Investigative',
    spawns: [coroner, detective, lookout, sheriff],
};

const townKilling = {
    name: 'Town Killing',
    spawns: [bodyguard, jailor, veteran, vigilante],
};

const townPower = {
    name: 'Town Power',
    spawns: [busDriver, jailor, spy, veteran],
};

const townProtective = {
    name: 'Town Protective',
    spawns: [armorsmith, bodyguard, busDriver, doctor, stripper],
};

const townRandom = {
    name: 'Town Random',
    spawns: [
        armorsmith, bodyguard, busDriver, citizen, coroner, detective, doctor, lookout, jailor,
        marshall, masonLeader, mason, mayor, sheriff, snitch, spy, stripper, vigilante, veteran,
    ],
};

const schema = {
    description: slogan,
    enemies: [[town, outcasts], [town, mafia], [town, cult], [town, threat], [mafia, cult],
        [mafia, threat], [cult, threat]],
    factions: [benigns, cult, mafia, outcasts, town, threat],
    factionRoles: {
        [benigns.name]: [
            amnesiac, executioner, jester, survivor,
        ],
        [cult.name]: [
            cultist, cultLeader,
        ],
        [mafia.name]: [
            agent, blackmailer, coward, disguiser, framer, godfather, goon, investigator, jailor,
            janitor, stripper,
        ],
        [outcasts.name]: [
            elector, witch,
        ],
        [threat.name]: [
            arsonist, massMurderer, poisoner, serialKiller,
        ],
        [town.name]: [
            armorsmith, bodyguard, busDriver, citizen, coroner, detective, doctor, jailor, lookout,
            marshall, mason, masonLeader, mayor, sheriff, snitch, spy, stripper, veteran, vigilante,
        ],
    },
    hiddens: [
        benignsRandom,
        mafiaRandom,
        neutralEvilRandom,
        threatRandom,
        townGovernment, townInvestigative, townKilling, townProtective, townPower, townRandom,
    ],
    featuredKey: 'sc2933',
    featuredPriority: 21,
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: false,
    }, {
        name: setupModifiers.EXECUTIONER_WIN_IMMUNE,
        value: false,
    }, {
        name: setupModifiers.FOLLOW_GETS_ALL,
        value: false,
    }, {
        name: setupModifiers.MASON_PROMOTION,
        value: true,
    }, {
        name: setupModifiers.MASON_NON_CIT_RECRUIT,
        value: false,
    }, {
        name: setupModifiers.CONVERT_REFUSABLE,
        value: false,
    }, {
        name: setupModifiers.CULT_KEEPS_ROLES,
        value: false,
    }, {
        name: setupModifiers.CORONER_LEARNS_ROLES,
        value: true,
    }, {
        name: setupModifiers.CIT_RATIO,
        value: 0,
    }],
    name: 'Sc2Mafia\'s 933',
    roles: [
        agent, amnesiac, armorsmith, arsonist, blackmailer, bodyguard, busDriver, citizen, coroner,
        coward, cultist, cultLeader, detective, disguiser, elector, executioner, doctor, framer,
        godfather, goon, investigator, jailor, janitor, jester, lookout, marshall, mason,
        masonLeader, massMurderer, mayor, poisoner, serialKiller, sheriff, snitch, spy, stripper,
        survivor, veteran, vigilante, witch,
    ],
    setupHiddens: [
        godfather,
        mafiaRandom,
        townInvestigative,
        townProtective,
        townRandom,
        townRandom,
        townRandom,
        getSetupHidden(benignsRandom, 8, 8),
        getSetupHidden(neutralEvilRandom, 9, 9),
        getSetupHidden(townRandom, 9, 12),
        getSetupHidden(threatRandom, 10),
        getSetupHidden(benignsRandom, 10, 10),
        getSetupHidden(neutralEvilRandom, 11, 11),
        getSetupHidden(townRandom, 11),
        getSetupHidden(mafiaRandom, 12),
        getSetupHidden(townKilling, 12),
        getSetupHidden(benignsRandom, 13, 13),
        getSetupHidden(townGovernment, 13),
        getSetupHidden(neutralEvilRandom, 14),
        getSetupHidden(townRandom, 14),
        getSetupHidden(benignsRandom, 15),
        getSetupHidden(benignsRandom, 16),
    ],
    sheriffDetectables: {
        [town.name]: [mafia],
    },
    slogan,
    winPriority: [threat, mafia, cult, outcasts, town, benigns],
};

module.exports = {
    schema,
};
