const { getSetupHidden } = require('../util/playerCountHelpers');

const factionModifier = require('../constants/factionModifiers');
const abilityModifiers = require('../constants/abilityModifiers');
const roleModifiers = require('../constants/roleModifiers');
const setupModifiers = require('../constants/setupModifiers');

const {
    red,
} = require('../constants/colors');

const {
    mafia: mafiaBase, town,
    cult: cultBase,
    outcasts: outcastBase,
    benigns: benignBase,
} = require('./definitions/baseFactions');

const {
    architect, busDriver, investigator, agent, drugDealer, framer, janitor,
    jailor,
    coroner,
    bodyguard,
    citizen: citizenBase,
    doctor,
    detective,
    lookout,
    witch,
    sheriff,
    vigilante,
    amnesiac,
    ghost,
    executioner,
    jester,
} = require('./definitions/baseRoles');


const slogan = "Clementine's Dystopian Mafia and Cult Game";

const unrecruitableModifiers = modifiers => [
    ...modifiers.filter(modifier => modifier.name !== factionModifier.IS_RECRUITABLE),
    { name: factionModifier.IS_RECRUITABLE, value: false },
];

const mafia = {
    ...mafiaBase,
    name: 'Soma Dealer',
    color: red,
    modifiers: unrecruitableModifiers(mafiaBase.modifiers),
};

const cult = {
    ...cultBase,
    name: 'Savage',
    modifiers: unrecruitableModifiers(cultBase.modifiers),
};

const benigns = {
    ...benignBase,
    name: 'Epsilon',
};

const threat = {
    ...outcastBase,
    modifiers: unrecruitableModifiers(outcastBase.modifiers),
};

const modifiers = [{
    name: setupModifiers.DAY_START,
    value: true,
}, {
    name: setupModifiers.CHARGE_VARIABILITY,
    value: 0,
}, {
    name: setupModifiers.DIFFERENTIATED_FACTION_KILLS,
    value: true,
}, {
    name: setupModifiers.GS_DAY_GUNS,
    value: false,
}, {
    name: setupModifiers.ENFORCER_LEARNS_NAMES,
    value: true,
}, {
    name: setupModifiers.CORONER_EXHUMES,
    value: true,
}, {
    name: setupModifiers.BREAD_PASSING,
    value: true,
}, {
    name: setupModifiers.MAYOR_VOTE_POWER,
    value: 3,
}, {
    name: setupModifiers.SELF_BREAD_USAGE,
    value: false,
    minPlayerCount: 0,
    maxPlayerCount: 24,
}, {
    name: setupModifiers.SELF_BREAD_USAGE,
    value: true,
    minPlayerCount: 25,
}, {
    name: setupModifiers.HEAL_BLOCKS_POISON,
    value: true,
}, {
    name: setupModifiers.GUARD_REDIRECTS_CONVERT,
    value: true,
}, {
    name: setupModifiers.GUARD_REDIRECTS_POISON,
    value: true,
}, {
    name: setupModifiers.GUARD_REDIRECTS_DOUSE,
    value: true,
}, {
    name: setupModifiers.CULT_POWER_ROLE_CD,
    value: 0,
}, {
    name: setupModifiers.CULT_KEEPS_ROLES,
    value: false,
}, {
    name: setupModifiers.CONVERT_REFUSABLE,
    value: false,
}, {
    name: setupModifiers.CULT_PROMOTION,
    value: true,
}, {
    name: setupModifiers.GD_REANIMATE,
    value: false,
}, {
    name: setupModifiers.JANITOR_GETS_ROLES,
    value: true,
}, {
    name: setupModifiers.ARCH_BURN,
    value: true,
}, {
    name: setupModifiers.ARSON_DAY_IGNITES,
    value: -1,
}, {
    name: setupModifiers.AMNESIAC_MUST_REMEMBER,
    value: true,
}, {
    name: setupModifiers.JESTER_CAN_ANNOY,
    value: true,
}, {
    name: setupModifiers.JESTER_KILLS,
    value: 25,
}, {
    name: setupModifiers.EXEC_TOWN,
    value: true,
}, {
    name: setupModifiers.PROXY_UPGRADE,
    value: true,
}, {
    name: setupModifiers.BREAD_NAME,
    value: 'soma',
}, {
    name: setupModifiers.CIT_RATIO,
    value: 66,
    minPlayerCount: 0,
    maxPlayerCount: 15,
}, {
    name: setupModifiers.CIT_RATIO,
    value: 50,
    minPlayerCount: 16,
    maxPlayerCount: 19,
}, {
    name: setupModifiers.CIT_RATIO,
    value: 40,
    minPlayerCount: 20,
    maxPlayerCount: 22,
}, {
    name: setupModifiers.CIT_RATIO,
    value: 43,
    minPlayerCount: 23,
    maxPlayerCount: 28,
}, {
    name: setupModifiers.CIT_RATIO,
    value: 38,
    minPlayerCount: 29,
    maxPlayerCount: 30,
}, {
    name: setupModifiers.CIT_RATIO,
    value: 33,
    minPlayerCount: 31,
    maxPlayerCount: 33,
}, {
    name: setupModifiers.CIT_RATIO,
    value: 44,
    minPlayerCount: 34,
}];

const stripper = {
    name: 'Stripper',
    abilities: ['Block'],
    modifiers: [{
        name: roleModifiers.UNBLOCKABLE,
        value: true,
    }],
};

const disguiser = {
    name: 'Disguiser',
    abilities: [{
        name: 'Disguiser',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 1,
        }],
    }],
};

const survivor = {
    name: 'Survivor',
    abilities: [{
        name: 'Survivor',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 2,
            minPlayerCount: 0,
            maxPlayerCount: 23,
        }, {
            name: abilityModifiers.CHARGES,
            value: 3,
            minPlayerCount: 24,
            maxPlayerCount: 29,
        }, {
            name: abilityModifiers.CHARGES,
            value: 4,
            minPlayerCount: 30,
        }],
    }],
};

const godfather = {
    name: 'Godfather',
    abilities: ['Godfather'],
    modifiers: [{
        name: roleModifiers.UNDETECTABLE,
        value: true,
    }, {
        name: roleModifiers.UNIQUE,
        value: true,
    }, {
        name: roleModifiers.AUTO_VEST,
        value: 1,
        minPlayerCount: 20,
    }],
};

const graveDigger = {
    name: 'Grave Digger',
    abilities: [{
        name: 'GraveDigger',
        modifiers: [{
            name: abilityModifiers.COOLDOWN,
            value: 1,
        }],
    }],
};

const interceptor = {
    name: 'Interceptor',
    abilities: ['Interceptor'],
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }],
};

const mafiaJailor = {
    ...jailor,
    abilityModifiers: {
        Jailor: [{
            name: abilityModifiers.CHARGES,
            value: 2,
        }],
    },
};

const poisoner = {
    name: 'Poisoner',
    abilities: [{
        name: 'Poisoner',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 2,
        }],
    }],
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }],
};

const hiddenSoma = {
    name: 'Hidden Soma Dealer',
    spawns: [agent, drugDealer, framer, janitor, graveDigger, interceptor, jailor, poisoner,
        architect, busDriver, investigator, stripper],
};

const savageLeader = {
    name: 'Savage Leader',
    abilities: [{
        name: 'CultLeader',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 2,
            minPlayerCount: 0,
            maxPlayerCount: 21,
        }, {
            name: abilityModifiers.CHARGES,
            value: 3,
            minPlayerCount: 22,
            maxPlayerCount: 24,
        }, {
            name: abilityModifiers.CHARGES,
            value: 4,
            minPlayerCount: 25,
            maxPlayerCount: 32,
        }, {
            name: abilityModifiers.CHARGES,
            value: 5,
            minPlayerCount: 33,
        }],
    }, {
        name: 'ProxyKill',
    }, {
        name: 'TeamTakedown',
    }],
    modifiers: [{
        name: roleModifiers.AUTO_VEST,
        value: 1,
        minPlayerCount: 20,
    }],
};

const blackSmith = {
    name: 'Blacksmith',
    abilities: ['Armorsmith', 'Gunsmith'],
};

const clubber = {
    name: 'Clubber',
    abilities: [{
        name: 'Clubber',
    }, {
        name: 'Baker',
        modifiers: [{
            name: abilityModifiers.COOLDOWN,
            value: 1,
        }],
    }],
    modifiers: [{
        name: roleModifiers.UNCONVERTABLE,
        value: true,
    }],
};

const delta = {
    ...citizenBase,
    name: 'Delta',
};

const enforcer = {
    name: 'Enforcer',
    abilities: [{
        name: 'Enforcer',
    }, {
        name: 'Baker',
        modifiers: [{
            name: abilityModifiers.COOLDOWN,
            value: 1,
        }],
    }],
    modifiers: [{
        name: roleModifiers.UNCONVERTABLE,
        value: true,
    }],
};

const veteran = {
    name: 'Veteran',
    abilities: ['Veteran'],
};

const mayor = {
    name: 'Mayor',
    abilities: [{
        name: 'Mayor',
    }, {
        name: 'Baker',
        modifiers: [{
            name: abilityModifiers.COOLDOWN,
            value: 1,
        }],
    }],
    modifiers: [{
        name: roleModifiers.UNCONVERTABLE,
        value: true,
    }, {
        name: roleModifiers.UNIQUE,
        value: true,
    }],
};

const townInvestigator = { role: investigator, faction: town };
const townArchitect = { role: architect, faction: town };
const townBusdriver = { role: busDriver, faction: town };
const townJailor = { faction: town, role: jailor };
const townStripper = { role: stripper, faction: town };

const hiddenTown = {
    name: 'Hidden Town',
    spawns: [townArchitect, blackSmith, bodyguard, townInvestigator,
        townBusdriver, coroner, delta, detective,
        doctor, enforcer, lookout, sheriff, townStripper, vigilante,
        veteran, townJailor, { ...clubber, minPlayerCount: 20 }],
};

const hiddenAlpha = {
    name: 'Hidden Alpha',
    spawns: [mayor, enforcer, { ...clubber, minPlayerCount: 20 }],
};

const hiddenBeta = {
    name: 'Hidden Beta',
    spawns: [
        townArchitect, blackSmith, bodyguard, doctor, townBusdriver, townJailor, townStripper,
        vigilante, veteran,
    ],
};

const hiddenGamma = {
    name: 'Hidden Gamma',
    spawns: [
        townInvestigator, coroner, detective, lookout, sheriff,
    ],
};

const armsDealer = {
    name: 'Arms Dealer',
    abilities: ['Blacksmith'],
};

const arsonist = {
    name: 'Arsonist',
    abilities: ['Douse', 'Undouse', 'Burn'],
    modifiers: [{
        name: roleModifiers.AUTO_VEST,
        value: 1,
    }],
};

const massMurderer = {
    name: 'Mass Murderer',
    abilities: ['MassMurderer'],
    modifiers: [{
        name: roleModifiers.AUTO_VEST,
        value: 1,
    }],
};

const devourer = {
    ...jailor,
    abilityModifiers: {
        Jailor: [{
            name: abilityModifiers.JAILOR_CLEAN_ROLES,
            value: 2,
        }],
    },
};

const serialKiller = {
    name: 'Serial Killer',
    abilities: ['Serial Killer'],
    modifiers: [{
        name: roleModifiers.AUTO_VEST,
        value: 1,
    }, {
        name: roleModifiers.UNBLOCKABLE,
        value: true,
    }],
};

const hiddenEvilNeutrals = {
    name: 'Hidden Threat',
    spawns: [armsDealer, arsonist, { role: jailor, faction: threat }, witch,
        serialKiller, massMurderer],
};

const hiddenEpsilon = {
    name: 'Hidden Epsilon',
    spawns: [amnesiac, ghost, executioner, jester, survivor],
};

const schema = {
    description: slogan,
    enemies: [[cult, town], [town, mafia], [threat, town], [cult, mafia]],
    factions: [mafia, town, cult, benigns, threat],
    factionRoles: {
        [benigns.name]: [ghost, amnesiac, survivor, jester, executioner],
        [threat.name]: [armsDealer, arsonist, massMurderer, devourer, serialKiller, witch],
        [cult.name]: [savageLeader],
        [town.name]: [
            jailor, stripper, architect, delta, blackSmith, bodyguard, busDriver, clubber,
            investigator,
            detective, doctor, lookout, enforcer, mayor, sheriff, vigilante, veteran, coroner,
        ],
        [mafia.name]: [disguiser, godfather, graveDigger, interceptor, mafiaJailor, poisoner,
            agent, drugDealer, framer, janitor, architect, investigator, busDriver, stripper],
    },
    featuredKey: 'brave',
    featuredPriority: 20,
    hiddens: [hiddenSoma, hiddenTown, hiddenEvilNeutrals, hiddenAlpha, hiddenBeta, hiddenGamma,
        hiddenEpsilon],
    modifiers,
    name: 'Brave New World',
    roles: [
        amnesiac, ghost, executioner, jester, survivor,
        armsDealer, arsonist, massMurderer, serialKiller, witch,
        architect, busDriver, investigator, stripper, jailor, mayor, sheriff, vigilante, veteran,
        blackSmith, bodyguard, clubber, coroner, delta,
        doctor, detective, lookout, enforcer,
        janitor, savageLeader,
        disguiser, godfather, graveDigger, interceptor, poisoner, agent, framer, drugDealer,
    ],
    setupHiddens: [
        delta,
        delta,
        delta,
        delta,
        hiddenAlpha,
        hiddenBeta,
        hiddenGamma,
        hiddenTown,
        hiddenTown,
        hiddenTown,
        hiddenSoma,
        hiddenSoma,
        getSetupHidden(hiddenSoma, 0, 16),
        savageLeader,
        hiddenEpsilon,
        getSetupHidden(hiddenTown, 16),
        getSetupHidden(delta, 17),
        getSetupHidden(godfather, 17),
        getSetupHidden(delta, 18),
        getSetupHidden(hiddenEvilNeutrals, 19),
        getSetupHidden(hiddenTown, 20),
        getSetupHidden(hiddenTown, 21),
        getSetupHidden(hiddenSoma, 22),
        getSetupHidden(hiddenTown, 23),
        getSetupHidden(delta, 24),
        getSetupHidden(hiddenSoma, 25),
        getSetupHidden(hiddenGamma, 26),
        getSetupHidden(delta, 27),
        getSetupHidden(hiddenEpsilon, 28),
        getSetupHidden(hiddenTown, 29),
        getSetupHidden(delta, 30),
        getSetupHidden(hiddenTown, 31),
        getSetupHidden(hiddenAlpha, 32),
        getSetupHidden(hiddenSoma, 33),
        getSetupHidden(hiddenBeta, 34),
        getSetupHidden(delta, 35),
        getSetupHidden(hiddenEpsilon, 36),
    ],
    sheriffDetectables: {
        [town.name]: [threat, cult, mafia],
    },
    slogan,
    winPriority: [[cult, mafia, threat], [town, benigns]],
};

module.exports = {
    schema,
};
