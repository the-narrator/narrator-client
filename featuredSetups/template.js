const { getSetupHidden } = require('../util/playerCountHelpers');

const setupModifiers = require('../constants/setupModifiers');

const { citizen } = require('./definitions/baseRoles');


const schema = {
    description: '',
    enemies: [],
    factions: [],
    factionRoles: {},
    featuredKey: '',
    featuredPriority: 1,
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: true,
    }],
    name: '',
    roles: [],
    setupHiddens: [
        getSetupHidden(citizen, 1),
    ],
    slogan: '',
    winPriority: [],
};

module.exports = {
    schema,
};
