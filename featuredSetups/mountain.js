const { getSetupHidden } = require('../util/playerCountHelpers');

const setupModifiers = require('../constants/setupModifiers');

const { town, mafia } = require('./definitions/baseFactions');

const { citizen, goon } = require('./definitions/baseRoles');


const schema = {
    description: 'Only goons and villagers',
    enemies: [[town, mafia]],
    factions: [town, mafia],
    factionRoles: {
        [town.name]: [citizen],
        [mafia.name]: [goon],
    },
    featuredKey: 'mountain',
    featuredPriority: 18,
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: true,
    }],
    name: 'Mountainous',
    roles: [citizen, goon],
    setupHiddens: [citizen, citizen, citizen, citizen, goon,
        getSetupHidden(citizen, 6),
        getSetupHidden(citizen, 7),
        getSetupHidden(citizen, 8),
        getSetupHidden(citizen, 9),
        getSetupHidden(citizen, 10),
        getSetupHidden(goon, 11),
        getSetupHidden(citizen, 12),
        getSetupHidden(citizen, 13),
        getSetupHidden(citizen, 14),
        getSetupHidden(goon, 15),
        getSetupHidden(citizen, 16),
        getSetupHidden(citizen, 17),
        getSetupHidden(citizen, 18),
        getSetupHidden(citizen, 19),
        getSetupHidden(citizen, 20),
    ],
    slogan: 'Only goons and villagers',
    winPriority: [mafia, town],
};

module.exports = {
    schema,
};
