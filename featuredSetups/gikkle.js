const abilityModifiers = require('../constants/abilityModifiers');
const roleModifiers = require('../constants/roleModifiers');
const setupModifiers = require('../constants/setupModifiers');

const {
    town,
    cult,
    mafia,
    threat,
} = require('./definitions/baseFactions');

const {
    elector, graveDigger, interceptor, ventriloquist, witch,
    ghost, executioner, jester, amnesiac,
    architect, busDriver, operator, stripper,
    armorsmith, baker, bodyguard, bulletproof, clubber, coroner,
    detective, doctor, enforcer, gunsmith, lookout, mayor, miller, parityCop, sheriff,
    snitch, spy,
    agent, assassin, voteStopper, drugDealer, framer, investigator, janitor, silencer,
} = require('./definitions/baseRoles');


const slogan = "Gikkle's setup!";
const description = 'Gikkle\'s setup';

const jailor = {
    name: 'Jailor',
    abilities: [{
        name: 'JailExecute',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const vigilante = {
    name: 'Vigilante',
    abilities: [{
        name: 'Vigilante',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 2,
        }],
    }],
};

const veteran = {
    name: 'Veteran',
    abilities: [{
        name: 'Veteran',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const commuter = {
    name: 'Commuter',
    abilities: [{
        name: 'Commuter',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const townRandom = {
    name: 'Hidden Town',
    spawns: [baker, bodyguard, commuter, coroner, detective, doctor,
        lookout, miller, parityCop, sheriff, snitch, spy, operator, veteran,
        armorsmith, vigilante, gunsmith, mayor,
        ...[
            architect, busDriver, stripper, jailor,
        ].map(role => ({ role, faction: town })),
    ],
};

const modifiers = [{
    name: setupModifiers.DAY_START,
    value: true,
}, {
    name: setupModifiers.FOLLOW_GETS_ALL,
    value: false,
}];

const tailor = {
    name: 'Tailor',
    abilities: [{
        name: 'Tailor',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const coward = {
    name: 'Coward',
    abilities: [{
        name: 'Coward',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const disguiser = {
    name: 'Disguiser',
    abilities: [{
        name: 'Disguiser',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const mafiaRandom = {
    name: 'Hidden Mafia',
    spawns: [assassin, agent, voteStopper, drugDealer, framer, investigator, janitor, silencer,
        tailor, coward, disguiser,
        architect, busDriver, stripper],
};

const blacksmith = {
    name: 'Blacksmith',
    abilities: [{
        name: 'Blacksmith',
        modifiers: [{
            name: abilityModifiers.GS_FAULTY_GUNS,
            value: true,
        }, {
            name: abilityModifiers.AS_FAKE_VESTS,
            value: true,
        }],
    }],
};

const cultLeader = {
    name: 'Cult Leader',
    abilities: [{
        name: 'CultLeader',
        modifiers: [{
            name: abilityModifiers.COOLDOWN,
            value: 1,
        }, {
            name: abilityModifiers.CHARGES,
            value: 2,
            minPlayerCount: 0,
            maxPlayerCount: 11,
        }, {
            name: abilityModifiers.CHARGES,
            value: 3,
            minPlayerCount: 12,
        }],
    }],
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }, {
        name: roleModifiers.AUTO_VEST,
        value: 1,
    }],
};

const serialKiller = {
    name: 'Serial Killer',
    abilities: ['SerialKiller', 'Bulletproof'],
};

const arsonist = {
    name: 'Arsonist',
    abilities: ['Douse', 'Undouse', 'Burn', 'Bulletproof'],
};

const massMurderer = {
    name: 'Mass Murderer',
    abilities: ['MassMurderer', 'Bulletproof'],
};

const joker = {
    name: 'Joker',
    abilities: ['Joker', 'Bulletproof'],
};

const electromaniac = {
    name: 'Electro Maniac',
    abilities: ['ElectroManiac', 'Bulletproof'],
};

const poisoner = {
    name: 'Poisoner',
    abilities: ['Poisoner', 'Bulletproof'],
};

const hiddenNeutral = {
    name: 'Hidden Neutral',
    spawns: [cultLeader, joker, poisoner, serialKiller, massMurderer, arsonist, electromaniac,
        elector, graveDigger, interceptor, ventriloquist, witch, blacksmith,
        { faction: threat, role: operator }],
};

const schema = {
    description,
    enemies: [[town, mafia], [town, cult], [town, threat],
        [mafia, cult], [mafia, threat], [cult, threat]],
    factions: [town, cult, mafia, threat],
    factionRoles: {
        [threat.name]: [
            joker, electromaniac, poisoner, massMurderer, arsonist, serialKiller,
            blacksmith, operator, elector, ventriloquist, witch, graveDigger, interceptor,
        ],
        [cult.name]: [cultLeader],
        [town.name]: [vigilante, veteran, commuter,
            architect, armorsmith, baker, busDriver, bodyguard, bulletproof, clubber,
            coroner, detective, doctor, enforcer, gunsmith, jailor, lookout, mayor, miller,
            operator, parityCop, sheriff, snitch, spy, stripper],
        [mafia.name]: [tailor, disguiser, coward,
            assassin, agent, voteStopper, drugDealer, framer, investigator, janitor, silencer,
            architect, busDriver, stripper],
    },
    featuredKey: 'gikkle',
    featuredPriority: 100,
    hiddens: [townRandom, mafiaRandom, hiddenNeutral],
    modifiers,
    name: 'Gikkle Setup',
    roles: [
        serialKiller, arsonist, massMurderer, electromaniac, poisoner, joker,
        elector, graveDigger, interceptor, ventriloquist, witch,
        cultLeader,
        blacksmith,
        ghost, executioner, amnesiac, jester,
        architect, busDriver, jailor, operator, stripper,
        vigilante, veteran, commuter,
        spy, snitch, sheriff, parityCop, miller, mayor, lookout,
        gunsmith, enforcer, doctor, detective, coroner, clubber, bulletproof, bodyguard,
        baker, armorsmith, tailor,
        coward, disguiser,
        agent, assassin, voteStopper, investigator, framer, drugDealer, janitor, silencer,
    ],
    sheriffDetectables: {
        [town.name]: [mafia, cult, threat],
    },
    setupHiddens: [
        mafiaRandom,
        mafiaRandom,
        townRandom,
        townRandom,
        townRandom,
        townRandom,
        townRandom,
        townRandom,
        hiddenNeutral,
    ],
    slogan,
    winPriority: [threat, mafia, cult, town],
};

module.exports = {
    schema,
};
