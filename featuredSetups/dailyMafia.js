const { getSetupHidden } = require('../util/playerCountHelpers');

const abilityModifiers = require('../constants/abilityModifiers');
const roleModifiers = require('../constants/roleModifiers');
const setupModifiers = require('../constants/setupModifiers');

const {
    citizen, goon, parityCop: parityCopBase,
} = require('./definitions/baseRoles');

const { mafia: baseMafia, town } = require('./definitions/baseFactions');


const slogan = 'Daily Mafia setup';

const mafia = {
    ...baseMafia,
    abilities: [{
        name: 'FactionKill',
        modifiers: [{
            name: abilityModifiers.ZERO_WEIGHTED,
            value: true,
        }],
    }, {
        name: 'SerialKiller',
        modifiers: [{
            name: abilityModifiers.FACTION_COUNT_MIN,
            value: 3,
        }, {
            name: abilityModifiers.ZERO_WEIGHTED,
            value: true,
        }],
    },
    ],
};

const doctor = {
    name: 'Doctor',
    abilities: [{
        name: 'Doctor',
        modifiers: [{
            name: abilityModifiers.BACK_TO_BACK,
            value: false,
        }],
    }],
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }],
};

const parityCop = {
    ...parityCopBase,
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }],
};

const vigilante = {
    name: 'Vigilante',
    abilities: [{
        name: 'Vigilante',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 1,
        }],
    }],
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }],
};

const goonSetupHidden = {
    ...goon,
    mustSpawn: true,
};

const townPowerRole = {
    name: 'Town Power Role',
    spawns: [vigilante, doctor, parityCop],
};

const schema = {
    description: slogan,
    enemies: [[town, mafia]],
    factions: [town, mafia],
    factionRoles: {
        [mafia.name]: [goon],
        [town.name]: [citizen, doctor, parityCop, vigilante],
    },
    featuredKey: 'daily',
    featuredPriority: 22,
    hiddens: [townPowerRole],
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: false,
    }, {
        name: setupModifiers.LAST_WILL,
        value: false,
    }, {
        name: setupModifiers.HEAL_FEEDBACK,
        value: false,
    }, {
        name: setupModifiers.HEAL_SUCCESS_FEEDBACK,
        value: false,
    }],
    name: 'Daily Mafia setup',
    roles: [
        citizen, doctor, goon, parityCop, vigilante,
    ].map(r => ({
        ...r,
        modifiers: [
            ...(r.modifiers || []),
            { name: roleModifiers.HIDDEN_FLIP, value: true },
        ],
    })),
    setupHiddens: [
        citizen,
        citizen,
        citizen,
        citizen,
        citizen,
        citizen,
        citizen,
        citizen,
        citizen,
        goonSetupHidden,
        goonSetupHidden,
        goonSetupHidden,
        getSetupHidden(townPowerRole, 0, 13),
        {
            ...getSetupHidden(townPowerRole, 0, 13),
            mustSpawn: true,
        }, {
            ...getSetupHidden(townPowerRole, 0, 13),
            mustSpawn: true,
        },
        getSetupHidden(doctor, 14),
        getSetupHidden(parityCop, 14),
        getSetupHidden(vigilante, 14),
    ],
    slogan,
    winPriority: [[mafia], [town]],
};

module.exports = {
    schema,
};
