const { getSetupHidden } = require('../util/playerCountHelpers');

const abilityModifiers = require('../constants/abilityModifiers');
const roleModifiers = require('../constants/roleModifiers');
const setupModifiers = require('../constants/setupModifiers');

const {
    benigns,
    town,
    outcasts,
    cult,
    mafia,
    threat,
} = require('./definitions/baseFactions');

const {
    elector, graveDigger, interceptor, ventriloquist, witch,
    ghost, executioner, jester, amnesiac,
    architect, busDriver, operator, stripper,
    citizen, armorsmith, baker, bodyguard, bulletproof, clubber, coroner,
    detective, doctor, enforcer, gunsmith, lookout, mayor, mason, miller, parityCop, sheriff,
    snitch, spy,
    agent, assassin, voteStopper, drugDealer, framer, investigator, janitor, silencer,
} = require('./definitions/baseRoles');


const slogan = "Voss's recommend setup. All roles enabled!";
const description = 'All roles enabled!';

const sleepwalker = {
    name: 'Sleepwalker',
    abilities: ['Sleepwalker'],
    receivedFactionRole: citizen,
};

const jailor = {
    name: 'Jailor',
    abilities: [{
        name: 'JailExecute',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const vigilante = {
    name: 'Vigilante',
    abilities: [{
        name: 'Vigilante',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 2,
        }],
    }],
};

const veteran = {
    name: 'Veteran',
    abilities: [{
        name: 'Veteran',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const commuter = {
    name: 'Commuter',
    abilities: [{
        name: 'Commuter',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const masonLeader = {
    name: 'Mason Leader',
    abilities: [{
        name: 'MasonLeader',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 4,
        }],
    }],
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }],
};

const townRandom = {
    name: 'Hidden Town',
    spawns: [baker, bodyguard, citizen, commuter, coroner, detective, doctor,
        lookout, miller, parityCop, sheriff, sleepwalker, snitch, spy, operator, veteran,
        armorsmith, vigilante, gunsmith, mayor, mason, masonLeader,
        ...[
            architect, busDriver, stripper, jailor,
        ].map(role => ({ role, faction: town })),
        ...[
            clubber, enforcer, bulletproof,
        ].map(role => ({ ...role, minPlayerCount: 10 })),
    ],
};

const modifiers = [{
    name: setupModifiers.CIT_RATIO,
    value: 45,
}, {
    name: setupModifiers.DAY_START,
    value: true,
}, {
    name: setupModifiers.FOLLOW_GETS_ALL,
    value: false,
}];

const tailor = {
    name: 'Tailor',
    abilities: [{
        name: 'Tailor',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const coward = {
    name: 'Coward',
    abilities: [{
        name: 'Coward',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const disguiser = {
    name: 'Disguiser',
    abilities: [{
        name: 'Disguiser',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
        }],
    }],
};

const mafiaRandom = {
    name: 'Hidden Mafia',
    spawns: [assassin, agent, voteStopper, drugDealer, framer, investigator, janitor, silencer,
        tailor, coward, disguiser,
        architect, busDriver, jailor, stripper],
};

const survivor = {
    name: 'Survivor',
    abilities: [{
        name: 'Survivor',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 4,
        }],
    }],
};

const benignsHidden = {
    name: 'Benigns Hidden',
    spawns: [ghost, amnesiac, survivor, jester, executioner],
};

const blacksmith = {
    name: 'Blacksmith',
    abilities: [{
        name: 'Blacksmith',
        modifiers: [{
            name: abilityModifiers.GS_FAULTY_GUNS,
            value: true,
        }, {
            name: abilityModifiers.AS_FAKE_VESTS,
            value: true,
        }],
    }],
};

const cultLeader = {
    name: 'Cult Leader',
    abilities: [{
        name: 'CultLeader',
        modifiers: [{
            name: abilityModifiers.COOLDOWN,
            value: 1,
        }, {
            name: abilityModifiers.CHARGES,
            value: 2,
            minPlayerCount: 0,
            maxPlayerCount: 11,
        }, {
            name: abilityModifiers.CHARGES,
            value: 3,
            minPlayerCount: 12,
        }],
    }],
    modifiers: [{
        name: roleModifiers.UNIQUE,
        value: true,
    }, {
        name: roleModifiers.AUTO_VEST,
        value: 1,
    }],
};

const outcastRandom = {
    name: 'Hidden Outcast',
    spawns: [elector, graveDigger, interceptor, ventriloquist, witch, blacksmith,
        { faction: outcasts, role: operator }],
};

const serialKiller = {
    name: 'Serial Killer',
    abilities: ['SerialKiller', 'Bulletproof'],
};

const arsonist = {
    name: 'Arsonist',
    abilities: ['Douse', 'Undouse', 'Burn', 'Bulletproof'],
};

const massMurderer = {
    name: 'Mass Murderer',
    abilities: ['MassMurderer', 'Bulletproof'],
};

const joker = {
    name: 'Joker',
    abilities: ['Joker', 'Bulletproof'],
};

const electromaniac = {
    name: 'Electro Maniac',
    abilities: ['ElectroManiac', 'Bulletproof'],
};

const poisoner = {
    name: 'Poisoner',
    abilities: ['Poisoner', 'Bulletproof'],
};

const hiddenThreat = {
    name: 'Hidden Threat',
    spawns: [cultLeader, joker, poisoner, serialKiller, massMurderer, arsonist, electromaniac],
};

const schema = {
    description,
    enemies: [[town, mafia], [town, outcasts], [town, cult], [town, threat],
        [mafia, cult], [mafia, threat], [cult, threat]],
    factions: [benigns, town, outcasts, cult, mafia, threat],
    factionRoles: {
        [threat.name]: [joker, electromaniac, poisoner, massMurderer, arsonist, serialKiller],
        [cult.name]: [cultLeader],
        [outcasts.name]:
            [blacksmith, operator, elector, ventriloquist, witch, graveDigger, interceptor]
                .map(role => ({
                    ...role,
                    roleModifiers: [{
                        name: roleModifiers.AUTO_VEST,
                        value: 1,
                    }],
                })),
        [benigns.name]: [executioner, ghost, survivor, amnesiac, jester],
        [town.name]: [vigilante, veteran, commuter, masonLeader,
            architect, armorsmith, baker, citizen, busDriver, bodyguard, bulletproof, clubber,
            coroner, detective, doctor, enforcer, gunsmith, jailor, lookout, mayor, mason, miller,
            operator, parityCop, sheriff, sleepwalker, snitch, spy, stripper],
        [mafia.name]: [tailor, disguiser, coward,
            assassin, agent, voteStopper, drugDealer, framer, investigator, janitor, silencer,
            architect, busDriver, jailor, stripper],
    },
    featuredKey: 'mash',
    featuredPriority: 9,
    hiddens: [townRandom, mafiaRandom, hiddenThreat, benignsHidden, outcastRandom],
    modifiers,
    name: 'Narrator Mash',
    roles: [
        serialKiller, arsonist, massMurderer, electromaniac, poisoner, joker,
        elector, graveDigger, interceptor, ventriloquist, witch,
        cultLeader,
        blacksmith,
        survivor, ghost, executioner, amnesiac, jester,
        architect, busDriver, jailor, operator, stripper,
        vigilante, veteran, commuter, masonLeader,
        spy, snitch, sleepwalker, sheriff, parityCop, miller, mason, mayor, lookout,
        gunsmith, enforcer, doctor, detective, coroner, clubber, bulletproof, bodyguard, citizen,
        baker, armorsmith, tailor,
        coward, disguiser,
        agent, assassin, voteStopper, investigator, framer, drugDealer, janitor, silencer,
    ],
    sheriffDetectables: {
        [town.name]: [mafia, cult, threat],
    },
    setupHiddens: [
        mafiaRandom,
        mafiaRandom,
        townRandom,
        townRandom,
        townRandom,
        townRandom,
        townRandom,
        getSetupHidden(benignsHidden, 8, 8),
        getSetupHidden(outcastRandom, 9, 9),
        getSetupHidden(townRandom, 9),
        getSetupHidden(hiddenThreat, 10),
        getSetupHidden(benignsHidden, 10, 10),
        getSetupHidden(outcastRandom, 11, 11),
        getSetupHidden(townRandom, 11),
        getSetupHidden(mafiaRandom, 12),
        getSetupHidden(townRandom, 12),
        getSetupHidden(benignsHidden, 13, 13),
        getSetupHidden(outcastRandom, 14),
        getSetupHidden(townRandom, 14),
        getSetupHidden(benignsHidden, 15),
        getSetupHidden(benignsHidden, 16),
    ],
    slogan,
    winPriority: [threat, mafia, cult, outcasts, town, benigns],
};

module.exports = {
    schema,
};
