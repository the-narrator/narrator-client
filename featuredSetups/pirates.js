const { getSetupHidden } = require('../util/playerCountHelpers');

const factionModifier = require('../constants/factionModifiers');
const abilityModifiers = require('../constants/abilityModifiers');
const roleModifiers = require('../constants/roleModifiers');
const setupModifiers = require('../constants/setupModifiers');

const {
    yellow, brown, red, purple, blue, green,
} = require('../constants/colors');

const {
    cult: cultBase, benigns,
    mafia: mafiaBase, threat: threatBase,
    town: townBase,
} = require('./definitions/baseFactions');

const {
    citizen,
    coroner,
    doctor,
    lookout,
    detective,
    jailor,
    enforcer,
    bodyguard,
    mayor: mayorBase,
    stripper,
    drugDealer,
    witch,
    coward,
    spy,
    agent,
    architect,
    ventriloquist,
    investigator,
    framer,
    blackmailer,
    executioner,
    jester,
    ghost,
    amnesiac,
} = require('./definitions/baseRoles');


const unrecruitableModifiers = modifiers => [
    ...modifiers.filter(modifier => modifier.name !== factionModifier.IS_RECRUITABLE),
    { name: factionModifier.IS_RECRUITABLE, value: false },
];

const pirates = {
    ...mafiaBase,
    name: 'Pirate',
    color: red,
    description: 'A group of real pirates with intentions to kill and steal!',
    modifiers: unrecruitableModifiers(mafiaBase.modifiers),
};

const cp9 = {
    ...mafiaBase,
    name: 'CP9',
    color: purple,
    // eslint-disable-next-line max-len
    description: 'A group of dangerous cyborg assassins sent by the World Government.  Their orders are to kill every inhabitant of the island.',
    modifiers: unrecruitableModifiers(mafiaBase.modifiers),
};

const threat = {
    ...threatBase,
    name: 'Threat',
    color: brown,
    description: 'An insane fellow who just wants to watch the island burn.',
    modifiers: unrecruitableModifiers(threatBase.modifiers),
};

const rebels = {
    ...cultBase,
    name: 'Rebel',
    color: yellow,
    description: 'An evergrowing group, with their cell deep in the society of the islanders.',
    modifiers: unrecruitableModifiers(cultBase.modifiers),
};

const marines = {
    ...cultBase,
    name: 'Marine',
    color: blue,
    description: 'Local authorities with a battleship.',
    modifiers: unrecruitableModifiers(cultBase.modifiers),
};

const village = {
    ...townBase,
    name: 'Islander',
    color: green,
    description: 'A carefree group of people living on Cinco Island.',
};

const mayor = {
    ...mayorBase,
    modifiers: [
        ...mayorBase.modifiers,
        { name: roleModifiers.UNCONVERTABLE, value: true },
    ],
};

const busDriver = {
    name: 'Bus Driver',
    abilities: [{
        name: 'Driver',
        modifiers: [{
            name: abilityModifiers.SELF_TARGET,
            value: false,
        }],
    }],
};

const veteran = {
    name: 'Veteran',
    abilities: [{
        name: 'Veteran',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 1,
            minPlayerCount: 0,
            maxPlayerCount: 19,
        }, {
            name: abilityModifiers.CHARGES,
            value: 2,
            minPlayerCount: 20,
            maxPlayerCount: 28,
        }, {
            name: abilityModifiers.CHARGES,
            value: 3,
            minPlayerCount: 29,
        }],
    }],
};

const blackSmith = {
    name: 'Blacksmith',
    abilities: ['Armorsmith', 'Gunsmith'],
};

const hiddenVillage = {
    name: 'Hidden Islander',
    spawns: [mayor, doctor, coroner, busDriver, blackSmith, stripper, veteran],
};

const navyAdmiral = {
    name: 'Navy Admiral',
    abilities: ['Sheriff'],
};

const vigilante = {
    name: 'Vigilante',
    abilities: [{
        name: 'Vigilante',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 1,
            minPlayerCount: 0,
            maxPlayerCount: 21,
        }, {
            name: abilityModifiers.CHARGES,
            value: 2,
            minPlayerCount: 22,
            maxPlayerCount: 26,
        }, {
            name: abilityModifiers.CHARGES,
            value: 3,
            minPlayerCount: 27,
        }],
    }],
};

const hiddenMarines = {
    name: 'Hidden Marine',
    spawns: [detective, lookout, bodyguard, jailor, enforcer, { ...vigilante, minPlayerCount: 18 }],
};

const infiltrator = {
    name: 'Infiltrator',
    abilities: ['Infiltrator'],
    modifiers: [{
        name: roleModifiers.AUTO_VEST,
        value: 1,
        minPlayerCount: 24,
        maxPlayerCount: 28,
    }, {
        name: roleModifiers.AUTO_VEST,
        value: 1,
        minPlayerCount: 31,
    }],
};

const hiddenCp9 = {
    name: 'Hidden CP9',
    spawns: [witch, drugDealer, coward, spy, agent, architect, ventriloquist],
};

const threatVest = [{
    name: roleModifiers.AUTO_VEST,
    value: 1,
}];

const serialKiller = {
    name: 'Serial Killer',
    abilities: ['SerialKiller'],
    modifiers: threatVest,
};

const poisoner = {
    name: 'Poisoner',
    abilities: ['Poisoner'],
    threatVest,
};

const arsonist = {
    name: 'Arsonist',
    abilities: ['Undouse', 'Douse', 'Burn'],
    modifiers: threatVest,
};

const electromaniac = {
    name: 'Electro Maniac',
    abilities: ['ElectroManiac'],
    modifiers: threatVest,
};

const joker = {
    name: 'Joker',
    abilities: ['Joker'],
    modifiers: threatVest,
};

const hiddenThreat = {
    name: 'Hidden Threat',
    spawns: [serialKiller, arsonist, joker, poisoner, electromaniac],
    modifiers: threatVest,
};

const pirateCaptain = {
    name: 'Pirate Captain',
    abilities: ['Godfather'],
    modifiers: [{
        name: roleModifiers.UNDETECTABLE,
        value: true,
    }, {
        name: roleModifiers.AUTO_VEST,
        value: 1,
        minPlayerCount: 26,
        maxPlayerCount: 29,
    }, {
        name: roleModifiers.AUTO_VEST,
        value: 1,
        minPlayerCount: 33,
    }],
};

const disguiser = {
    name: 'Disguiser',
    abilities: [{
        name: 'Disguiser',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 2,
        }],
    }],
};

const janitor = {
    name: 'Janitor',
    abilities: [{
        name: 'Janitor',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 4,
        }],
    }],
};

const hiddenPirate = {
    name: 'Hidden Pirate',
    spawns: [{ role: stripper, faction: pirates },
        janitor, investigator, framer, blackmailer, disguiser],
};

const rebelLeader = {
    name: 'Rebel Leader',
    abilities: [{
        name: 'CultLeader',
        modifiers: [{
            name: abilityModifiers.CHARGES,
            value: 3,
            minPlayerCount: 0,
            maxPlayerCount: 15,
        }, {
            name: abilityModifiers.CHARGES,
            value: 4,
            minPlayerCount: 16,
            maxPlayerCount: 23,
        }, {
            name: abilityModifiers.CHARGES,
            value: 5,
            minPlayerCount: 24,
            maxPlayerCount: 26,
        }, {
            name: abilityModifiers.CHARGES,
            value: 6,
            minPlayerCount: 27,
            maxPlayerCount: 30,
        }, {
            name: abilityModifiers.CHARGES,
            value: 7,
            minPlayerCount: 31,
            maxPlayerCount: 32,
        }, {
            name: abilityModifiers.CHARGES,
            value: 8,
            minPlayerCount: 33,
            maxPlayerCount: 34,
        }, {
            name: abilityModifiers.CHARGES,
            value: 9,
            minPlayerCount: 35,
        }],
    }, {
        name: 'FactionKill',
        modifiers: [{
            name: abilityModifiers.FACTION_COUNT_MIN,
            value: 2,
            minPlayerCount: 0,
            maxPlayerCount: 18,
        }, {
            name: abilityModifiers.FACTION_COUNT_MIN,
            value: 3,
            minPlayerCount: 19,
            maxPlayerCount: 19,
        }, {
            name: abilityModifiers.FACTION_COUNT_MIN,
            value: 4,
            minPlayerCount: 20,
            maxPlayerCount: 31,
        }, {
            name: abilityModifiers.FACTION_COUNT_MIN,
            value: 5,
            minPlayerCount: 32,
        }],
    }],
};

const scout = {
    name: 'Scout',
    abilities: ['Scout'],
};

const hiddenBenign = {
    name: 'Hidden Benign',
    spawns: [executioner, ghost, jester, amnesiac],
};

const slogan = 'Frago\'s 5 factioned \'Pirates\' Game';

const schema = {
    description: slogan,
    enemies: [[village, threat], [village, pirates], [village, cp9],
        [marines, rebels], [pirates, marines], [rebels, cp9]],
    factions: [pirates, marines, threat, cp9, rebels, village, benigns],
    factionRoles: {
        [benigns.name]: [executioner, jester, ghost, amnesiac],
        [rebels.name]: [rebelLeader, scout],
        [pirates.name]: [
            pirateCaptain, disguiser, janitor, framer, blackmailer, investigator, stripper,
        ],
        [marines.name]: [navyAdmiral, vigilante, lookout, detective, jailor, enforcer, bodyguard],
        [village.name]: [mayor, blackSmith, busDriver, stripper, citizen, doctor, coroner, veteran],
        [cp9.name]: [infiltrator, drugDealer, witch, coward, spy, agent, architect, ventriloquist],
        [threat.name]: [serialKiller, poisoner, arsonist, electromaniac, joker],
    },
    featuredKey: 'pirates',
    featuredPriority: 19,
    hiddens: [hiddenVillage, hiddenBenign, hiddenMarines, hiddenPirate, hiddenThreat, hiddenCp9],
    modifiers: [{
        name: setupModifiers.DAY_START,
        value: true,
    }, {
        name: setupModifiers.DIFFERENTIATED_FACTION_KILLS,
        value: true,
    }, {
        name: setupModifiers.GS_DAY_GUNS,
        value: false,
    }, {
        name: setupModifiers.ENFORCER_LEARNS_NAMES,
        value: true,
    }, {
        name: setupModifiers.CORONER_LEARNS_ROLES,
        value: false,
    }, {
        name: setupModifiers.MAYOR_VOTE_POWER,
        value: 2,
    }, {
        name: setupModifiers.CHECK_DIFFERENTIATION,
        value: true,
    }, {
        name: setupModifiers.AMNESIAC_MUST_REMEMBER,
        value: true,
    }, {
        name: setupModifiers.AMNESIAC_MUST_REMEMBER,
        value: true,
    }, {
        name: setupModifiers.ARSON_DAY_IGNITES,
        value: 0,
    }, {
        name: setupModifiers.WITCH_FEEDBACK,
        value: true,
    }, {
        name: setupModifiers.SPY_TARGETS_ALLIES,
        value: true,
    }, {
        name: setupModifiers.SPY_TARGETS_ENEMIES,
        value: false,
    }, {
        name: setupModifiers.JANITOR_GETS_ROLES,
        value: false,
    }, {
        name: setupModifiers.CULT_POWER_ROLE_CD,
        value: 0,
    }, {
        name: setupModifiers.CONVERT_REFUSABLE,
        value: true,
    }, {
        name: setupModifiers.CULT_PROMOTION,
        value: false,
    }, {
        name: setupModifiers.CULT_KEEPS_ROLES,
        value: true,
    }, {
        name: setupModifiers.BOUNTY_INCUBATION,
        value: 2,
    }, {
        name: setupModifiers.BOUNTY_FAIL_REWARD,
        value: 2,
    }, {
        name: setupModifiers.ARCH_QUARANTINE,
        value: true,
    }, {
        name: setupModifiers.JESTER_CAN_ANNOY,
        value: true,
    }, {
        name: setupModifiers.JESTER_KILLS,
        value: 14,
    }],
    name: 'Treasure Island',
    roles: [
        mayor, stripper, citizen, busDriver, blackSmith, doctor, coroner, veteran,
        navyAdmiral, vigilante, lookout, detective, jailor, enforcer, bodyguard,
        infiltrator, ventriloquist, architect, agent, spy, coward, witch, drugDealer,
        serialKiller, poisoner, arsonist, electromaniac, joker,
        pirateCaptain, disguiser, janitor, framer, blackmailer, investigator,
        rebelLeader, scout,
        executioner, jester, amnesiac, ghost,
    ],
    sheriffDetectables: {
        [marines.name]: [pirates, rebels, threat, cp9],
    },
    slogan,
    setupHiddens: [
        pirateCaptain,
        hiddenPirate,
        infiltrator,
        hiddenCp9,
        rebelLeader,
        navyAdmiral,
        hiddenMarines,
        doctor,
        hiddenVillage,
        hiddenVillage,
        citizen,
        citizen,
        citizen,
        citizen,
        getSetupHidden(citizen, 15),
        getSetupHidden(citizen, 16),
        getSetupHidden(hiddenBenign, 17, 19),
        getSetupHidden(hiddenPirate, 18),
        getSetupHidden(citizen, 19, 19),
        getSetupHidden(scout, 20),
        getSetupHidden(hiddenMarines, 20),
        getSetupHidden(hiddenCp9, 20),
        getSetupHidden(citizen, 21),
        getSetupHidden(hiddenBenign, 22, 22),
        getSetupHidden(hiddenMarines, 23),
        getSetupHidden(hiddenThreat, 23),
        getSetupHidden(citizen, 24),
        getSetupHidden(hiddenBenign, 25),
        getSetupHidden(hiddenVillage, 26),
        getSetupHidden(citizen, 26),
        getSetupHidden(citizen, 27),
        getSetupHidden(citizen, 28),
        getSetupHidden(hiddenCp9, 29),
        getSetupHidden(hiddenPirate, 30),
        getSetupHidden(hiddenVillage, 31),
        getSetupHidden(citizen, 32),
        getSetupHidden(citizen, 33),
        getSetupHidden(citizen, 34),
        getSetupHidden(citizen, 35),
        getSetupHidden(citizen, 36),
    ],
    winPriority: [[pirates, cp9, threat], [rebels, marines], village, benigns],
};

module.exports = {
    schema,
};
