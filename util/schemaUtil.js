const { keyBy } = require('lodash');

const factionService = require('../services/factionService');
const factionAbilityService = require('../services/factionAbilityService');
const factionRoleService = require('../services/factionRoleService');
const roleAbilityService = require('../services/roleAbilityService');
const roleService = require('../services/roleService');
const setupService = require('../services/setupService');

const helpers = require('../util/helpers');


async function createFactions(setup, schemaFactions){
    await factionService.deleteAll(setup.factions.map(f => f.id));
    return Promise.all(
        schemaFactions.map(f => createFaction({ ...f, setupID: setup.id })),
    );
}

async function createFactionRoles(factions, roles, schema){
    const roleMap = helpers.makeMap(roles, 'name');
    const factionMap = helpers.makeMap(factions, 'name');
    const factionNames = Object.keys(schema.factionRoles);
    const listOfLists = factionNames.map(factionName => schema.factionRoles[factionName]
        .map(role => createFactionRole(roleMap, role, factionMap, factionName)));
    const factionRoles = await Promise.all(helpers.flattenList(listOfLists));
    const factionRolesMap = helpers.makeMap(factionRoles, 'name');
    await Promise.all(factionNames.map(factionName => Promise.all(schema.factionRoles[factionName]
        .filter(role => role.receivedFactionRole)
        .map(role => factionRoleService.setReceived(
            factionRolesMap[role.name].id,
            factionRolesMap[role.receivedFactionRole.name].id,
        )))));
    return factionRoles;
}

async function createFactionRole(roleMap, role, factionMap, factionName){
    const factionRole = await factionService.createFactionRole(
        roleMap[role.name].id,
        factionMap[factionName].id,
    );
    if(role.roleModifiers)
        await Promise.all(role.roleModifiers.map(modifier => factionRoleService.modifyRole(
            factionRole.id, modifier,
        )));
    if(role.abilityModifiers)
        await modifyFactionRoleAbilities(roleMap, role, factionRole.id);
    return factionRole;
}

function modifyFactionRoleAbilities(roleMap, role, factionRoleID){
    return Promise.all(Object.entries(role.abilityModifiers).map(([abilityName, modifiers]) => {
        const baseRole = roleMap[role.name];
        const ability = baseRole.abilities.find(a => a.name === abilityName);
        return Promise.all(modifiers.map(modifier => factionRoleService.modifyAbility(
            factionRoleID,
            ability.id,
            modifier,
        )));
    }));
}

async function createRoles(setup, schemaRoles){
    await roleService.deleteAll(setup.roles.map(r => r.id));
    return Promise.all(schemaRoles.map(r => createRole({ ...r, setupID: setup.id })));
}

function getOrCreateSetup(setups, schema){
    const setup = setups.find(s => s.name === schema.name && s.id && s.isEditable);
    if(setup)
        return setupService.getByID(setup.id);
    return setupService.create(schema);
}

async function setSheriffDetectables(factions, sheriffDetectablesSchema){
    if(!sheriffDetectablesSchema)
        return;
    const factionMap = helpers.makeMap(factions, 'name');
    return Promise.all(
        Object.keys(sheriffDetectablesSchema).map(factionName => Promise.all(
            sheriffDetectablesSchema[factionName]
                .map(detectableFaction => factionService.addSheriffDetectable(
                    factionMap[factionName].id, factionMap[detectableFaction.name].id,
                )),
        )),
    );
}

module.exports = {
    createFactions,
    createFactionRoles,
    createRoles,
    getOrCreateSetup,
    setSheriffDetectables,
};

async function createFaction(schemaFaction){
    const faction = await factionService.createFactionShell(schemaFaction);
    if(schemaFaction.abilities)
        await Promise.all(schemaFaction.abilities
            .map(ability => addFactionAbility(faction.id, ability)));
    if(schemaFaction.modifiers)
        await Promise.all(schemaFaction.modifiers
            .map(modifier => factionService.modify(faction.id, modifier)));
    return faction;
}

async function createRole(roleSchema){
    const abilities = roleSchema.abilities
        .map(ability => (typeof(ability) === 'string' ? ability : ability.name));
    const role = await roleService.createRole({
        ...roleSchema,
        abilities,
    });
    if(roleSchema.modifiers)
        await Promise.all(
            roleSchema.modifiers.map(modifierArg => roleService.modify(role.id, modifierArg)),
        );
    const abilityMap = keyBy(role.abilities, 'name');
    await Promise.all(roleSchema.abilities
        .filter(ability => typeof(ability) === 'object' && ability.modifiers)
        .map(ability => Promise.all(ability.modifiers
            .map(modifier => roleAbilityService.modify(
                role.id, abilityMap[ability.name].id, modifier,
            )))));
    return role;
}

async function addFactionAbility(factionID, abilitySchema){
    const factionAbility = await factionService.addFactionAbility(factionID, abilitySchema.name);
    return Promise.all(abilitySchema.modifiers
        .map(modifier => factionAbilityService.modify(factionAbility.id, modifier)));
}
