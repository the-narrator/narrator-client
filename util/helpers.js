function flattenList(inputList){
    return inputList.reduce((acc, val) => acc.concat(val), []);
}

function makeMap(elements, key){
    const map = {};
    elements.forEach(e => {
        map[e[key]] = e;
    });
    return map;
}


module.exports = {
    makeMap,
    flattenList,
};
