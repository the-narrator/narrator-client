function getSetupHidden(setupHidden, minPlayerCount = 0, maxPlayerCount = 255){
    return {
        ...setupHidden,
        minPlayerCount,
        maxPlayerCount,
    };
}

module.exports = {
    getSetupHidden,
};
