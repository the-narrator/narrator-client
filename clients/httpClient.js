const http = require('http');
const https = require('https');

const credentials = require('../credentials');
const config = require('../config');


let idToken = null;

function get(path){
    const options = {
        method: 'GET',
        port: config.port,
        host: config.host,
        path,
    };
    return sendNarratorRequest(options);
}

function post(path, body, headers){
    const options = {
        method: 'POST',
        port: config.port,
        host: config.host,
        path,
    };
    return sendNarratorRequest(options, body, headers);
}

function put(path, body){
    const options = {
        method: 'PUT',
        port: config.port,
        host: config.host,
        path,
    };
    return sendNarratorRequest(options, body);
}

function del(path){
    const options = {
        method: 'DELETE',
        port: config.port,
        host: config.host,
        path,
    };
    return sendNarratorRequest(options);
}

module.exports = {
    get,
    delete: del,
    post,
    put,
};

async function sendNarratorRequest(options, body, headers){
    const start = new Date().getTime();
    const auth = config.getEnv() === 'nofirebase' ? '' : await getAuthToken();
    const end = new Date().getTime();
    console.log(end - start);
    options.headers = headers || {
        auth,
        authtype: 'firebase',
    };
    return sendRequest(options, body);
}

async function getAuthToken(){
    if(idToken)
        return idToken;
    const response = await sendRequest({
        method: 'POST',
        port: 443,
        host: 'www.googleapis.com',
        path: '/identitytoolkit/v3/relyingparty/verifyPassword'
            + '?key=AIzaSyBqatywpJsXTXBmYP9p8eR-p3B0OWsmDTk',
        headers: {
            'Content-Type': 'application/json',
        },
    }, {
        email: credentials.firebaseEmail,
        password: credentials.firebasePassword,
        returnSecureToken: true,
    });
    idToken = JSON.parse(response).idToken;
    return idToken;
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

async function sendRequest(options, body){
    await new Promise(resolve => {
        setTimeout(resolve, getRandomArbitrary(5000, 120000));
    });
    return new Promise((resolve, reject) => {
        const client = options.port === 443 ? https : http;
        const req = client.request(options, resp => {
            let data = '';

            resp.on('data', chunk => {
                data += chunk;
            });

            resp.on('end', () => {
                if(resp.statusCode > 204)
                    // eslint-disable-next-line prefer-promise-reject-errors
                    reject({ options, data, body, statusCode: resp.statusCode });
                else {
                    resolve(data);
                }
            });
        });
        req.on('error', reject);
        if(body)
            req.write(JSON.stringify(body));
        req.end();
    });
}
